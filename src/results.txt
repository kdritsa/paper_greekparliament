---------------------------------------------------

*Generic Parliament Metrics (all times - all parties)*

Metrics: {'pos_sum': 556418, 'neg_sum': 356481, 'smog_Qmean': 14.76, 'anger_Qmean': 3.37, 'disgust_Qmean': 3.14, 'fear_Qmean': 2.18, 'happiness_Qmean': 2.04, 'sadness_Qmean': 1.23, 'surprise_Qmean': 3.58}

---------------------------------------------------

*Metrics in each of the 6 eras:*

Era 1989-1993: 
Metrics: {'pos_sum': 95531, 'neg_sum': 67808, 'smog_Qmean': 15.57, 'anger_Qmean': 3.46, 'disgust_Qmean': 3.21, 'fear_Qmean': 2.22, 'happiness_Qmean': 1.97, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.63}

In this era.......

Party: νεα δημοκρατια: 
Metrics: {'pos_sum': 50980, 'neg_sum': 33788, 'smog_Qmean': 15.55, 'anger_Qmean': 3.52, 'disgust_Qmean': 3.26, 'fear_Qmean': 2.25, 'happiness_Qmean': 1.98, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.68}

Party: πανελληνιο σοσιαλιστικο κινημα: 
Metrics: {'pos_sum': 33076, 'neg_sum': 25113, 'smog_Qmean': 15.45, 'anger_Qmean': 3.42, 'disgust_Qmean': 3.17, 'fear_Qmean': 2.19, 'happiness_Qmean': 1.98, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.6}

Party: οικολογοι εναλλακτικοι (ομοσπονδια οικολογικων εναλλακτικων οργανωσεων): 
Metrics: {'pos_sum': 326, 'neg_sum': 297, 'smog_Qmean': 16.14, 'anger_Qmean': 3.42, 'disgust_Qmean': 3.19, 'fear_Qmean': 2.23, 'happiness_Qmean': 1.86, 'sadness_Qmean': 1.25, 'surprise_Qmean': 3.56}

Party: συνασπισμος της αριστερας των κινηματων και της οικολογιας: 
Metrics: {'pos_sum': 7580, 'neg_sum': 5781, 'smog_Qmean': 16.33, 'anger_Qmean': 3.42, 'disgust_Qmean': 3.18, 'fear_Qmean': 2.18, 'happiness_Qmean': 1.94, 'sadness_Qmean': 1.19, 'surprise_Qmean': 3.59}

Party: ανεξαρτητοι (εκτος κομματος): 
Metrics: {'pos_sum': 1746, 'neg_sum': 1436, 'smog_Qmean': 15.75, 'anger_Qmean': 3.38, 'disgust_Qmean': 3.16, 'fear_Qmean': 2.18, 'happiness_Qmean': 2.01, 'sadness_Qmean': 1.24, 'surprise_Qmean': 3.59}

Party: κομμουνιστικο κομμα ελλαδας: 
Metrics: {'pos_sum': 1328, 'neg_sum': 1021, 'smog_Qmean': 15.69, 'anger_Qmean': 3.39, 'disgust_Qmean': 3.13, 'fear_Qmean': 2.16, 'happiness_Qmean': 1.92, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.53}

Party: πολιτικη ανοιξη: 
Metrics: {'pos_sum': 179, 'neg_sum': 148, 'smog_Qmean': 16.22, 'anger_Qmean': 3.36, 'disgust_Qmean': 3.07, 'fear_Qmean': 2.13, 'happiness_Qmean': 2.15, 'sadness_Qmean': 1.24, 'surprise_Qmean': 3.56}

Party: βουλη: 
Metrics: {'pos_sum': 7, 'neg_sum': 4, 'smog_Qmean': None, 'anger_Qmean': 3.72, 'disgust_Qmean': 3.25, 'fear_Qmean': 2.18, 'happiness_Qmean': 1.68, 'sadness_Qmean': 1.3, 'surprise_Qmean': 3.83}

Party: κομμουνιστικο κομμα ελλαδας εσωτερικου: 
Metrics: {'pos_sum': 171, 'neg_sum': 134, 'smog_Qmean': 15.35, 'anger_Qmean': 3.48, 'disgust_Qmean': 3.24, 'fear_Qmean': 2.2, 'happiness_Qmean': 2.04, 'sadness_Qmean': 1.09, 'surprise_Qmean': 3.61}

Party: πανελληνιο σοσιαλιστικο κινημα."Μα: 
Metrics: {'pos_sum': 1, 'neg_sum': 0, 'smog_Qmean': None, 'anger_Qmean': None, 'disgust_Qmean': None, 'fear_Qmean': None, 'happiness_Qmean': None, 'sadness_Qmean': None, 'surprise_Qmean': None}

Party: πανελληνιο σοσιαλιστικο κινημα."Μια και δεν έχουμε Κοινοβουλευτικό Εκπρόσωπο: 
Metrics: {'pos_sum': 1, 'neg_sum': 0, 'smog_Qmean': None, 'anger_Qmean': 3.14, 'disgust_Qmean': 2.92, 'fear_Qmean': 2.02, 'happiness_Qmean': 3.01, 'sadness_Qmean': 1.0, 'surprise_Qmean': 3.54}

Party: δημοκρατικη ανανεωση: 
Metrics: {'pos_sum': 136, 'neg_sum': 86, 'smog_Qmean': 14.61, 'anger_Qmean': 3.39, 'disgust_Qmean': 3.15, 'fear_Qmean': 2.18, 'happiness_Qmean': 2.01, 'sadness_Qmean': 1.25, 'surprise_Qmean': 3.53}

Era 1994-1998: 
Metrics: {'pos_sum': 66528, 'neg_sum': 43438, 'smog_Qmean': 15.45, 'anger_Qmean': 3.43, 'disgust_Qmean': 3.2, 'fear_Qmean': 2.22, 'happiness_Qmean': 2.03, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.61}

In this era.......

Party: νεα δημοκρατια: 
Metrics: {'pos_sum': 16091, 'neg_sum': 11867, 'smog_Qmean': 15.88, 'anger_Qmean': 3.42, 'disgust_Qmean': 3.19, 'fear_Qmean': 2.22, 'happiness_Qmean': 2.0, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.59}

Party: κομμουνιστικο κομμα ελλαδας: 
Metrics: {'pos_sum': 3319, 'neg_sum': 2627, 'smog_Qmean': 15.61, 'anger_Qmean': 3.49, 'disgust_Qmean': 3.26, 'fear_Qmean': 2.24, 'happiness_Qmean': 1.97, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.65}

Party: πανελληνιο σοσιαλιστικο κινημα: 
Metrics: {'pos_sum': 40170, 'neg_sum': 23595, 'smog_Qmean': 15.29, 'anger_Qmean': 3.45, 'disgust_Qmean': 3.22, 'fear_Qmean': 2.23, 'happiness_Qmean': 2.06, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.62}

Party: συνασπισμος της αριστερας των κινηματων και της οικολογιας: 
Metrics: {'pos_sum': 2202, 'neg_sum': 1821, 'smog_Qmean': 16.08, 'anger_Qmean': 3.29, 'disgust_Qmean': 3.08, 'fear_Qmean': 2.21, 'happiness_Qmean': 2.12, 'sadness_Qmean': 1.24, 'surprise_Qmean': 3.51}

Party: δημοκρατικο κοινωνικο κινημα: 
Metrics: {'pos_sum': 3022, 'neg_sum': 2238, 'smog_Qmean': 16.23, 'anger_Qmean': 3.41, 'disgust_Qmean': 3.18, 'fear_Qmean': 2.24, 'happiness_Qmean': 2.02, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.56}

Party: πολιτικη ανοιξη: 
Metrics: {'pos_sum': 1724, 'neg_sum': 1290, 'smog_Qmean': 15.27, 'anger_Qmean': 3.39, 'disgust_Qmean': 3.15, 'fear_Qmean': 2.16, 'happiness_Qmean': 1.98, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.59}

Party: βουλη: 
Metrics: {'pos_sum': 0, 'neg_sum': 0, 'smog_Qmean': None, 'anger_Qmean': 3.0, 'disgust_Qmean': 1.5, 'fear_Qmean': 1.0, 'happiness_Qmean': 1.0, 'sadness_Qmean': 2.0, 'surprise_Qmean': 3.5}

Era 1999-2003: 
Metrics: {'pos_sum': 97086, 'neg_sum': 63575, 'smog_Qmean': 15.53, 'anger_Qmean': 3.41, 'disgust_Qmean': 3.18, 'fear_Qmean': 2.2, 'happiness_Qmean': 2.05, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.59}

In this era.......

Party: νεα δημοκρατια: 
Metrics: {'pos_sum': 25689, 'neg_sum': 19746, 'smog_Qmean': 15.36, 'anger_Qmean': 3.38, 'disgust_Qmean': 3.16, 'fear_Qmean': 2.19, 'happiness_Qmean': 2.03, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.57}

Party: κομμουνιστικο κομμα ελλαδας: 
Metrics: {'pos_sum': 5724, 'neg_sum': 4076, 'smog_Qmean': 14.73, 'anger_Qmean': 3.43, 'disgust_Qmean': 3.2, 'fear_Qmean': 2.2, 'happiness_Qmean': 1.94, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.6}

Party: πανελληνιο σοσιαλιστικο κινημα: 
Metrics: {'pos_sum': 60702, 'neg_sum': 35658, 'smog_Qmean': 15.67, 'anger_Qmean': 3.43, 'disgust_Qmean': 3.2, 'fear_Qmean': 2.21, 'happiness_Qmean': 2.08, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.62}

Party: συνασπισμος της αριστερας των κινηματων και της οικολογιας: 
Metrics: {'pos_sum': 3095, 'neg_sum': 2598, 'smog_Qmean': 15.57, 'anger_Qmean': 3.3, 'disgust_Qmean': 3.11, 'fear_Qmean': 2.17, 'happiness_Qmean': 1.99, 'sadness_Qmean': 1.19, 'surprise_Qmean': 3.49}

Party: δημοκρατικο κοινωνικο κινημα: 
Metrics: {'pos_sum': 1786, 'neg_sum': 1437, 'smog_Qmean': 15.33, 'anger_Qmean': 3.4, 'disgust_Qmean': 3.17, 'fear_Qmean': 2.21, 'happiness_Qmean': 2.02, 'sadness_Qmean': 1.23, 'surprise_Qmean': 3.6}

Party: ανεξαρτητοι (εκτος κομματος): 
Metrics: {'pos_sum': 90, 'neg_sum': 60, 'smog_Qmean': 16.22, 'anger_Qmean': 3.06, 'disgust_Qmean': 2.91, 'fear_Qmean': 2.05, 'happiness_Qmean': 1.97, 'sadness_Qmean': 1.41, 'surprise_Qmean': 3.3}

Era 2004-2008: 
Metrics: {'pos_sum': 109293, 'neg_sum': 71888, 'smog_Qmean': 14.8, 'anger_Qmean': 3.33, 'disgust_Qmean': 3.11, 'fear_Qmean': 2.17, 'happiness_Qmean': 2.08, 'sadness_Qmean': 1.28, 'surprise_Qmean': 3.56}

In this era.......

Party: νεα δημοκρατια: 
Metrics: {'pos_sum': 58513, 'neg_sum': 36245, 'smog_Qmean': 14.94, 'anger_Qmean': 3.36, 'disgust_Qmean': 3.13, 'fear_Qmean': 2.18, 'happiness_Qmean': 2.11, 'sadness_Qmean': 1.28, 'surprise_Qmean': 3.59}

Party: πανελληνιο σοσιαλιστικο κινημα: 
Metrics: {'pos_sum': 36505, 'neg_sum': 25593, 'smog_Qmean': 15.09, 'anger_Qmean': 3.3, 'disgust_Qmean': 3.08, 'fear_Qmean': 2.17, 'happiness_Qmean': 2.06, 'sadness_Qmean': 1.27, 'surprise_Qmean': 3.53}

Party: λαικος ορθοδοξος συναγερμος: 
Metrics: {'pos_sum': 2495, 'neg_sum': 1562, 'smog_Qmean': 13.16, 'anger_Qmean': 3.3, 'disgust_Qmean': 3.09, 'fear_Qmean': 2.17, 'happiness_Qmean': 2.11, 'sadness_Qmean': 1.24, 'surprise_Qmean': 3.55}

Party: συνασπισμος ριζοσπαστικης αριστερας: 
Metrics: {'pos_sum': 4055, 'neg_sum': 3192, 'smog_Qmean': 13.84, 'anger_Qmean': 3.3, 'disgust_Qmean': 3.08, 'fear_Qmean': 2.18, 'happiness_Qmean': 2.1, 'sadness_Qmean': 1.3, 'surprise_Qmean': 3.52}

Party: κομμουνιστικο κομμα ελλαδας: 
Metrics: {'pos_sum': 7207, 'neg_sum': 4914, 'smog_Qmean': 13.38, 'anger_Qmean': 3.34, 'disgust_Qmean': 3.11, 'fear_Qmean': 2.15, 'happiness_Qmean': 1.95, 'sadness_Qmean': 1.28, 'surprise_Qmean': 3.53}

Party: ανεξαρτητοι (εκτος κομματος): 
Metrics: {'pos_sum': 344, 'neg_sum': 237, 'smog_Qmean': 17.04, 'anger_Qmean': 3.19, 'disgust_Qmean': 2.96, 'fear_Qmean': 2.13, 'happiness_Qmean': 2.22, 'sadness_Qmean': 1.17, 'surprise_Qmean': 3.47}

Party: συνασπισμος της αριστερας των κινηματων και της οικολογιας: 
Metrics: {'pos_sum': 174, 'neg_sum': 145, 'smog_Qmean': 17.19, 'anger_Qmean': 3.3, 'disgust_Qmean': 3.09, 'fear_Qmean': 2.15, 'happiness_Qmean': 1.93, 'sadness_Qmean': 1.06, 'surprise_Qmean': 3.51}

Era 2009-2013: 
Metrics: {'pos_sum': 107514, 'neg_sum': 63462, 'smog_Qmean': 14.29, 'anger_Qmean': 3.33, 'disgust_Qmean': 3.1, 'fear_Qmean': 2.15, 'happiness_Qmean': 2.06, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.55}

In this era.......

Party: ανεξαρτητοι ελληνες - πανος καμμενος: 
Metrics: {'pos_sum': 2874, 'neg_sum': 1758, 'smog_Qmean': 13.84, 'anger_Qmean': 3.32, 'disgust_Qmean': 3.1, 'fear_Qmean': 2.15, 'happiness_Qmean': 2.02, 'sadness_Qmean': 1.23, 'surprise_Qmean': 3.54}

Party: νεα δημοκρατια: 
Metrics: {'pos_sum': 26732, 'neg_sum': 15600, 'smog_Qmean': 14.4, 'anger_Qmean': 3.34, 'disgust_Qmean': 3.09, 'fear_Qmean': 2.14, 'happiness_Qmean': 2.06, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.56}

Party: συνασπισμος ριζοσπαστικης αριστερας: 
Metrics: {'pos_sum': 12129, 'neg_sum': 8766, 'smog_Qmean': 13.63, 'anger_Qmean': 3.35, 'disgust_Qmean': 3.11, 'fear_Qmean': 2.15, 'happiness_Qmean': 2.05, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.58}

Party: ανεξαρτητοι (εκτος κομματος): 
Metrics: {'pos_sum': 6652, 'neg_sum': 3931, 'smog_Qmean': 14.53, 'anger_Qmean': 3.31, 'disgust_Qmean': 3.08, 'fear_Qmean': 2.17, 'happiness_Qmean': 2.06, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.55}

Party: πανελληνιο σοσιαλιστικο κινημα: 
Metrics: {'pos_sum': 38458, 'neg_sum': 21888, 'smog_Qmean': 15.35, 'anger_Qmean': 3.36, 'disgust_Qmean': 3.13, 'fear_Qmean': 2.17, 'happiness_Qmean': 2.08, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.58}

Party: ανεξαρτητοι δημοκρατικοι βουλευτες: 
Metrics: {'pos_sum': 418, 'neg_sum': 211, 'smog_Qmean': 13.82, 'anger_Qmean': 3.26, 'disgust_Qmean': 3.04, 'fear_Qmean': 2.08, 'happiness_Qmean': 1.95, 'sadness_Qmean': 1.24, 'surprise_Qmean': 3.49}

Party: λαικος ορθοδοξος συναγερμος: 
Metrics: {'pos_sum': 9023, 'neg_sum': 4733, 'smog_Qmean': 13.42, 'anger_Qmean': 3.29, 'disgust_Qmean': 3.05, 'fear_Qmean': 2.11, 'happiness_Qmean': 2.13, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.53}

Party: λαικος συνδεσος - χρυση αυγη: 
Metrics: {'pos_sum': 1582, 'neg_sum': 1001, 'smog_Qmean': 12.75, 'anger_Qmean': 3.38, 'disgust_Qmean': 3.16, 'fear_Qmean': 2.16, 'happiness_Qmean': 1.96, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.58}

Party: κομμουνιστικο κομμα ελλαδας: 
Metrics: {'pos_sum': 7644, 'neg_sum': 4594, 'smog_Qmean': 12.6, 'anger_Qmean': 3.26, 'disgust_Qmean': 3.03, 'fear_Qmean': 2.07, 'happiness_Qmean': 1.88, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.44}

Party: δημοκρατικη αριστερα: 
Metrics: {'pos_sum': 1999, 'neg_sum': 980, 'smog_Qmean': 13.92, 'anger_Qmean': 3.27, 'disgust_Qmean': 3.04, 'fear_Qmean': 2.13, 'happiness_Qmean': 2.12, 'sadness_Qmean': 1.17, 'surprise_Qmean': 3.52}

Party: βουλη: 
Metrics: {'pos_sum': 3, 'neg_sum': 0, 'smog_Qmean': None, 'anger_Qmean': 3.14, 'disgust_Qmean': 2.92, 'fear_Qmean': 2.02, 'happiness_Qmean': 1.58, 'sadness_Qmean': 1.0, 'surprise_Qmean': 3.14}

Era 2014-2017: 
Metrics: {'pos_sum': 80466, 'neg_sum': 46310, 'smog_Qmean': 13.18, 'anger_Qmean': 3.29, 'disgust_Qmean': 3.04, 'fear_Qmean': 2.1, 'happiness_Qmean': 2.03, 'sadness_Qmean': 1.24, 'surprise_Qmean': 3.53}

In this era.......

Party: ανεξαρτητοι ελληνες - πανος καμμενος: 
Metrics: {'pos_sum': 1796, 'neg_sum': 1013, 'smog_Qmean': 13.35, 'anger_Qmean': 3.34, 'disgust_Qmean': 3.1, 'fear_Qmean': 2.15, 'happiness_Qmean': 1.95, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.54}

Party: νεα δημοκρατια: 
Metrics: {'pos_sum': 18859, 'neg_sum': 11385, 'smog_Qmean': 13.91, 'anger_Qmean': 3.31, 'disgust_Qmean': 3.05, 'fear_Qmean': 2.11, 'happiness_Qmean': 2.04, 'sadness_Qmean': 1.23, 'surprise_Qmean': 3.53}

Party: ανεξαρτητοι δημοκρατικοι βουλευτες: 
Metrics: {'pos_sum': 267, 'neg_sum': 103, 'smog_Qmean': None, 'anger_Qmean': 3.25, 'disgust_Qmean': 3.0, 'fear_Qmean': 2.08, 'happiness_Qmean': 2.15, 'sadness_Qmean': 1.11, 'surprise_Qmean': 3.45}

Party: συνασπισμος ριζοσπαστικης αριστερας: 
Metrics: {'pos_sum': 29275, 'neg_sum': 16843, 'smog_Qmean': 13.84, 'anger_Qmean': 3.31, 'disgust_Qmean': 3.07, 'fear_Qmean': 2.12, 'happiness_Qmean': 2.07, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.55}

Party: λαικος συνδεσος - χρυση αυγη: 
Metrics: {'pos_sum': 2529, 'neg_sum': 1509, 'smog_Qmean': 9.69, 'anger_Qmean': 3.25, 'disgust_Qmean': 2.95, 'fear_Qmean': 2.02, 'happiness_Qmean': 1.92, 'sadness_Qmean': 1.29, 'surprise_Qmean': 3.53}

Party: το ποταμι: 
Metrics: {'pos_sum': 3295, 'neg_sum': 1518, 'smog_Qmean': 11.98, 'anger_Qmean': 3.22, 'disgust_Qmean': 2.93, 'fear_Qmean': 2.04, 'happiness_Qmean': 2.02, 'sadness_Qmean': 1.31, 'surprise_Qmean': 3.47}

Party: δημοκρατικη αριστερα: 
Metrics: {'pos_sum': 958, 'neg_sum': 412, 'smog_Qmean': 10.27, 'anger_Qmean': 3.21, 'disgust_Qmean': 2.99, 'fear_Qmean': 2.07, 'happiness_Qmean': 1.93, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.49}

Party: δημοκρατικη συμπαραταξη (πανελληνιο σοσιαλιστικο κινημα - δημοκρατικη αριστερα): 
Metrics: {'pos_sum': 7696, 'neg_sum': 4645, 'smog_Qmean': 13.26, 'anger_Qmean': 3.25, 'disgust_Qmean': 2.99, 'fear_Qmean': 2.07, 'happiness_Qmean': 2.01, 'sadness_Qmean': 1.23, 'surprise_Qmean': 3.47}

Party: ανεξαρτητοι (εκτος κομματος): 
Metrics: {'pos_sum': 1808, 'neg_sum': 1086, 'smog_Qmean': 10.82, 'anger_Qmean': 3.3, 'disgust_Qmean': 3.03, 'fear_Qmean': 2.09, 'happiness_Qmean': 2.05, 'sadness_Qmean': 1.27, 'surprise_Qmean': 3.55}

Party: κομμουνιστικο κομμα ελλαδας: 
Metrics: {'pos_sum': 4905, 'neg_sum': 3082, 'smog_Qmean': 12.16, 'anger_Qmean': 3.32, 'disgust_Qmean': 3.03, 'fear_Qmean': 2.09, 'happiness_Qmean': 1.94, 'sadness_Qmean': 1.29, 'surprise_Qmean': 3.55}

Party: πανελληνιο σοσιαλιστικο κινημα: 
Metrics: {'pos_sum': 4329, 'neg_sum': 2583, 'smog_Qmean': 14.03, 'anger_Qmean': 3.34, 'disgust_Qmean': 3.09, 'fear_Qmean': 2.13, 'happiness_Qmean': 1.99, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.55}

Party: ενωση κεντρωων: 
Metrics: {'pos_sum': 1546, 'neg_sum': 698, 'smog_Qmean': 10.09, 'anger_Qmean': 3.23, 'disgust_Qmean': 2.89, 'fear_Qmean': 2.02, 'happiness_Qmean': 2.08, 'sadness_Qmean': 1.38, 'surprise_Qmean': 3.55}

Party: ανεξαρτητοι ελληνες εθνικη πατριωτικη δημοκρατικη συμμαχια: 
Metrics: {'pos_sum': 2581, 'neg_sum': 1060, 'smog_Qmean': 10.88, 'anger_Qmean': 3.25, 'disgust_Qmean': 2.99, 'fear_Qmean': 2.07, 'happiness_Qmean': 2.08, 'sadness_Qmean': 1.24, 'surprise_Qmean': 3.51}

Party: λαικη ενοτητα: 
Metrics: {'pos_sum': 622, 'neg_sum': 373, 'smog_Qmean': 13.9, 'anger_Qmean': 3.32, 'disgust_Qmean': 3.08, 'fear_Qmean': 2.1, 'happiness_Qmean': 2.1, 'sadness_Qmean': 1.3, 'surprise_Qmean': 3.56}

---------------------------------------------------

*Metrics of each political party throughout all times*

Party: ανεξαρτητοι ελληνες - πανος καμμενος: 
Metrics: {'pos_sum': 4670, 'neg_sum': 2771, 'smog_Qmean': 13.62, 'anger_Qmean': 3.33, 'disgust_Qmean': 3.1, 'fear_Qmean': 2.15, 'happiness_Qmean': 2.0, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.54}

Party: νεα δημοκρατια: 
Metrics: {'pos_sum': 196864, 'neg_sum': 128631, 'smog_Qmean': 15.04, 'anger_Qmean': 3.39, 'disgust_Qmean': 3.15, 'fear_Qmean': 2.19, 'happiness_Qmean': 2.05, 'sadness_Qmean': 1.23, 'surprise_Qmean': 3.59}

Party: συνασπισμος ριζοσπαστικης αριστερας: 
Metrics: {'pos_sum': 45459, 'neg_sum': 28801, 'smog_Qmean': 13.78, 'anger_Qmean': 3.32, 'disgust_Qmean': 3.08, 'fear_Qmean': 2.14, 'happiness_Qmean': 2.07, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.56}

Party: ανεξαρτητοι (εκτος κομματος): 
Metrics: {'pos_sum': 10640, 'neg_sum': 6750, 'smog_Qmean': 13.98, 'anger_Qmean': 3.32, 'disgust_Qmean': 3.08, 'fear_Qmean': 2.15, 'happiness_Qmean': 2.06, 'sadness_Qmean': 1.23, 'surprise_Qmean': 3.55}

Party: κομμουνιστικο κομμα ελλαδας: 
Metrics: {'pos_sum': 30127, 'neg_sum': 20314, 'smog_Qmean': 13.38, 'anger_Qmean': 3.35, 'disgust_Qmean': 3.11, 'fear_Qmean': 2.14, 'happiness_Qmean': 1.93, 'sadness_Qmean': 1.24, 'surprise_Qmean': 3.54}

Party: πανελληνιο σοσιαλιστικο κινημα: 
Metrics: {'pos_sum': 213240, 'neg_sum': 134430, 'smog_Qmean': 15.38, 'anger_Qmean': 3.39, 'disgust_Qmean': 3.16, 'fear_Qmean': 2.19, 'happiness_Qmean': 2.05, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.59}

Party: ανεξαρτητοι δημοκρατικοι βουλευτες: 
Metrics: {'pos_sum': 685, 'neg_sum': 314, 'smog_Qmean': 13.82, 'anger_Qmean': 3.26, 'disgust_Qmean': 3.02, 'fear_Qmean': 2.08, 'happiness_Qmean': 2.05, 'sadness_Qmean': 1.18, 'surprise_Qmean': 3.47}

Party: λαικος ορθοδοξος συναγερμος: 
Metrics: {'pos_sum': 11518, 'neg_sum': 6295, 'smog_Qmean': 13.36, 'anger_Qmean': 3.29, 'disgust_Qmean': 3.06, 'fear_Qmean': 2.12, 'happiness_Qmean': 2.13, 'sadness_Qmean': 1.21, 'surprise_Qmean': 3.53}

Party: λαικος συνδεσος - χρυση αυγη: 
Metrics: {'pos_sum': 4111, 'neg_sum': 2510, 'smog_Qmean': 10.23, 'anger_Qmean': 3.3, 'disgust_Qmean': 3.03, 'fear_Qmean': 2.07, 'happiness_Qmean': 1.93, 'sadness_Qmean': 1.26, 'surprise_Qmean': 3.55}

Party: συνασπισμος της αριστερας των κινηματων και της οικολογιας: 
Metrics: {'pos_sum': 13051, 'neg_sum': 10345, 'smog_Qmean': 16.15, 'anger_Qmean': 3.36, 'disgust_Qmean': 3.14, 'fear_Qmean': 2.19, 'happiness_Qmean': 1.99, 'sadness_Qmean': 1.2, 'surprise_Qmean': 3.54}

Party: δημοκρατικη αριστερα: 
Metrics: {'pos_sum': 2957, 'neg_sum': 1392, 'smog_Qmean': 11.97, 'anger_Qmean': 3.25, 'disgust_Qmean': 3.03, 'fear_Qmean': 2.11, 'happiness_Qmean': 2.06, 'sadness_Qmean': 1.18, 'surprise_Qmean': 3.51}

Party: το ποταμι: 
Metrics: {'pos_sum': 3295, 'neg_sum': 1518, 'smog_Qmean': 11.98, 'anger_Qmean': 3.22, 'disgust_Qmean': 2.93, 'fear_Qmean': 2.04, 'happiness_Qmean': 2.02, 'sadness_Qmean': 1.31, 'surprise_Qmean': 3.47}

Party: οικολογοι εναλλακτικοι (ομοσπονδια οικολογικων εναλλακτικων οργανωσεων): 
Metrics: {'pos_sum': 326, 'neg_sum': 297, 'smog_Qmean': 16.14, 'anger_Qmean': 3.42, 'disgust_Qmean': 3.19, 'fear_Qmean': 2.23, 'happiness_Qmean': 1.86, 'sadness_Qmean': 1.25, 'surprise_Qmean': 3.56}

Party: δημοκρατικο κοινωνικο κινημα: 
Metrics: {'pos_sum': 4808, 'neg_sum': 3675, 'smog_Qmean': 15.75, 'anger_Qmean': 3.41, 'disgust_Qmean': 3.18, 'fear_Qmean': 2.23, 'happiness_Qmean': 2.02, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.58}

Party: δημοκρατικη συμπαραταξη (πανελληνιο σοσιαλιστικο κινημα - δημοκρατικη αριστερα): 
Metrics: {'pos_sum': 7696, 'neg_sum': 4645, 'smog_Qmean': 13.26, 'anger_Qmean': 3.25, 'disgust_Qmean': 2.99, 'fear_Qmean': 2.07, 'happiness_Qmean': 2.01, 'sadness_Qmean': 1.23, 'surprise_Qmean': 3.47}

Party: πολιτικη ανοιξη: 
Metrics: {'pos_sum': 1903, 'neg_sum': 1438, 'smog_Qmean': 15.31, 'anger_Qmean': 3.39, 'disgust_Qmean': 3.14, 'fear_Qmean': 2.15, 'happiness_Qmean': 2.0, 'sadness_Qmean': 1.22, 'surprise_Qmean': 3.59}

Party: βουλη: 
Metrics: {'pos_sum': 10, 'neg_sum': 4, 'smog_Qmean': None, 'anger_Qmean': 3.59, 'disgust_Qmean': 3.07, 'fear_Qmean': 2.07, 'happiness_Qmean': 1.61, 'sadness_Qmean': 1.37, 'surprise_Qmean': 3.72}

Party: ενωση κεντρωων: 
Metrics: {'pos_sum': 1546, 'neg_sum': 698, 'smog_Qmean': 10.09, 'anger_Qmean': 3.23, 'disgust_Qmean': 2.89, 'fear_Qmean': 2.02, 'happiness_Qmean': 2.08, 'sadness_Qmean': 1.38, 'surprise_Qmean': 3.55}

Party: ανεξαρτητοι ελληνες εθνικη πατριωτικη δημοκρατικη συμμαχια: 
Metrics: {'pos_sum': 2581, 'neg_sum': 1060, 'smog_Qmean': 10.88, 'anger_Qmean': 3.25, 'disgust_Qmean': 2.99, 'fear_Qmean': 2.07, 'happiness_Qmean': 2.08, 'sadness_Qmean': 1.24, 'surprise_Qmean': 3.51}

Party: λαικη ενοτητα: 
Metrics: {'pos_sum': 622, 'neg_sum': 373, 'smog_Qmean': 13.9, 'anger_Qmean': 3.32, 'disgust_Qmean': 3.08, 'fear_Qmean': 2.1, 'happiness_Qmean': 2.1, 'sadness_Qmean': 1.3, 'surprise_Qmean': 3.56}

Party: κομμουνιστικο κομμα ελλαδας εσωτερικου: 
Metrics: {'pos_sum': 171, 'neg_sum': 134, 'smog_Qmean': 15.35, 'anger_Qmean': 3.48, 'disgust_Qmean': 3.24, 'fear_Qmean': 2.2, 'happiness_Qmean': 2.04, 'sadness_Qmean': 1.09, 'surprise_Qmean': 3.61}

Party: πανελληνιο σοσιαλιστικο κινημα."Μα: 
Metrics: {'pos_sum': 1, 'neg_sum': 0, 'smog_Qmean': None, 'anger_Qmean': None, 'disgust_Qmean': None, 'fear_Qmean': None, 'happiness_Qmean': None, 'sadness_Qmean': None, 'surprise_Qmean': None}

Party: πανελληνιο σοσιαλιστικο κινημα."Μια και δεν έχουμε Κοινοβουλευτικό Εκπρόσωπο: 
Metrics: {'pos_sum': 1, 'neg_sum': 0, 'smog_Qmean': None, 'anger_Qmean': 3.14, 'disgust_Qmean': 2.92, 'fear_Qmean': 2.02, 'happiness_Qmean': 3.01, 'sadness_Qmean': 1.0, 'surprise_Qmean': 3.54}

Party: δημοκρατικη ανανεωση: 
Metrics: {'pos_sum': 136, 'neg_sum': 86, 'smog_Qmean': 14.61, 'anger_Qmean': 3.39, 'disgust_Qmean': 3.15, 'fear_Qmean': 2.18, 'happiness_Qmean': 2.01, 'sadness_Qmean': 1.25, 'surprise_Qmean': 3.53}