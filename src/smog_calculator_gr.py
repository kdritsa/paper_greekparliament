#import re
import math
import invoice
from cltk.tokenize.sentence import TokenizeSentence
from cltk.tokenize.word import WordTokenizer
from cltk.corpus.utils.importer import CorpusImporter
from greek_accentuation.syllabify import syllabify


def smog_index(text):
    
    corpus_importer = CorpusImporter('greek')
    corpus_importer.import_corpus('greek_models_cltk')
    
#    empty_sentence = re.compile(r"(^\s*[α-ωά-ώΑ-ΩΆ-Ώ]{0,2}\s*[.;?]*$)")
    
    text = text.replace('!','.')
    
    tokenizer = TokenizeSentence('greek')
    sent_text = tokenizer.tokenize_sentences(text) # den pianei tis teleies otan einai kollhta oi protaseis xwris keno
        
    if len(sent_text)>=60:

        word_tokenizer = WordTokenizer('greek')
        sample_text = []
        
        part1 = sent_text[0:(int(len(sent_text)/3))]
        part2 = sent_text[((int(len(sent_text)/3))):(int(2/3*len(sent_text)))]
        part3 = sent_text[((int(2/3*len(sent_text)))):]
    
        first20 = part1[(int(len(part1)/2)-10):(int(len(part1)/2)+10)]
    
        middle20 = part2[(int(len(part2)/2)-10):(int(len(part2)/2)+10)]

        last20 = part3[(int(len(part3)/2)-10):(int(len(part3)/2)+10)]
        
        sample_text = first20 + middle20 + last20
    
        sample_text = [(' '.join(invoice.num_to_text(int(word)) if word.isdigit() else word for word in word_tokenizer.tokenize(sent))) for sent in sample_text]
    
        sent_num = len(sample_text)
        
        polysyllables=0
        all_words=0
        all_syll=0
        non_words = {'.', ',', ' '}

        for sent in sample_text:
            print(sent)
            for word in word_tokenizer.tokenize(sent):
                if word not in non_words:
                    all_words+=1
                    syllables = len(syllabify(word))
                    all_syll+=syllables
                    if syllables>2:
                        polysyllables+=1
        
        grade = 1.0430*math.sqrt(polysyllables*30/sent_num)+3.1291
        grade = int(grade * 100.0) / float(100.0) #up to second decimal
#        print('smog: '+str(grade))
    #    print('words: '+str(all_words))
    #    print('syllables: '+str(all_syll))
#        print('polysyllables: '+str(polysyllables))
        return(grade)

    else:
        print('Please, provide text with more than 60 sentences.')

