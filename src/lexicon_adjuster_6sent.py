# -*- coding: utf-8 -*-
import re
import csv
from collections import defaultdict

def avg_score(row, column1, column2, column3, column4):
    
    index1 = row[column1]
    index2 = row[column2]
    index3 = row[column3]
    index4 = row[column4]
    denominator=0
    if 'N/A' in index1:
        index1=0
    else:
        index1 = int(index1)
        denominator+=1
    if 'N/A' in index2:
        index2=0
    else:
        index2 = int(index2)
        denominator+=1
    if 'N/A' in index3:
        index3=0
    else:
        index3 = int(index3)
        denominator+=1
    if 'N/A' in index4:
        index4=0
    else:
        index4 = int(index4)
        denominator+=1
    if denominator!=0:
        index_avg = round(((index1+index2+index3+index4)/denominator),2)
    else:
        index_avg = 'N/A'
    return(index_avg)

in_lexicon = open('greek_sentiment_lexicon.tsv', 'r', encoding = 'utf-8')
in_reader = csv.DictReader(in_lexicon, dialect='excel-tab')

out_lexicon = open('out_lexicon_6sent.csv', 'w+', encoding = 'utf-8', newline='')
fieldnames = ['term', 'anger', 'disgust', 'fear', 'happiness', 'sadness', 'surprise']
out_writer = csv.DictWriter(out_lexicon, fieldnames=fieldnames)
out_writer.writeheader()

lexicon_dict = defaultdict(list)
unique_terms=[]

# Each row is an ordered dict
for row in in_reader:
    term = row['Term'].translate(str.maketrans('άέόώήίϊΐύϋΰ','αεοωηιιιυυυ')) #remove accents
    term = term.strip() # as it has some extra spaces at the end or beginning

    if (term!='') and (term not in unique_terms) and (not term.endswith('-')):

        avg_anger = avg_score(row, 'Anger1', 'Anger2', 'Anger3', 'Anger4')
        avg_disgust = avg_score(row, 'Disgust1', 'Disgust2', 'Disgust3', 'Disgust4')
        avg_fear = avg_score(row, 'Fear1', 'Fear2', 'Fear3', 'Fear4')
        avg_happiness = avg_score(row, 'Happiness1', 'Happiness2', 'Happiness3', 'Happiness4')
        avg_sadness = avg_score(row, 'Sadness1', 'Sadness2', 'Sadness3', 'Sadness4')
        avg_surprise = avg_score(row, 'Surprise1', 'Surprise2', 'Surprise3', 'Surprise4')
        
        # Skip case where all avg scores are equal to 'N/A'
        # With this check, from 2746 lexicon entries we go down to 2608 entries
        if not (all(avg=='N/A' for avg in [avg_anger,avg_disgust,avg_fear,avg_happiness,avg_sadness,avg_surprise])):

            # Add basic term to lexicon without suffixes
            if '-' in term: 
                term = re.sub('\s\s+' , ' ', term) #some entries have multiple spaces
                term = re.sub('(/ -)|-', '', term)
                term1, suffixes = term.split(' ')[0],set(term.split(' ')[1:])
            else: # skip splitting at whitespace words like "χαπι εντ"
                term1=term
                suffixes=[]
    
            if term1 not in unique_terms:
                unique_terms.append(term1)
                out_writer.writerow({'term': term1, 'anger': avg_anger, 'disgust': avg_disgust, 'fear': avg_fear, 'happiness': avg_happiness, 'sadness': avg_sadness, 'surprise': avg_surprise})
                lexicon_dict[term1] = [avg_anger, avg_disgust, avg_fear, avg_happiness, avg_sadness, avg_surprise]
    
            # Add term with different suffixes to lexicon
            base = term1[:-2]
            for suffix in suffixes:
                new_term = base+suffix
    
                if (new_term!='') and (new_term not in unique_terms):
                    unique_terms.append(new_term)
                    out_writer.writerow({'term': new_term, 'anger': avg_anger, 'disgust': avg_disgust, 'fear': avg_fear, 'happiness': avg_happiness, 'sadness': avg_sadness, 'surprise': avg_surprise})
                    lexicon_dict[new_term] = [avg_anger, avg_disgust, avg_fear, avg_happiness, avg_sadness, avg_surprise]


in_lexicon.close()
out_lexicon.close()

#The order of commands is chosen based on the fact that we had two entries like
# ευγενής and also ευγενής -ής -ές