# -*- coding: utf-8 -*-
from cltk.tokenize.word import WordTokenizer
from collections import defaultdict
import re
import csv
import operator
import pandas as pd

# pd.set_option('display.max_colwidth', -1)
word_tokenizer = WordTokenizer('greek')

word_freq=defaultdict(int)

df = pd.read_csv('../out_files/tell_all_speaker_per_sitting.csv',encoding='utf-8', header=None)

df_speeches = df.drop([0,1,2,3], axis=1) #keep only speeches column

# Format text of column with speeches that has column name 4
df_speeches[4] = (df_speeches[4].apply(lambda x: x.translate(str.maketrans('άέόώήίϊΐύϋΰ','αεοωηιιιυυυ'))))
df_speeches[4] = (df_speeches[4].apply(lambda x: (re.sub("[.,!;():?«»]",' ',x)).lower()))
df_speeches[4] = (df_speeches[4].apply(lambda x: re.sub("\s\s+" , ' ', x)))

all_all=''
for index, row in df_speeches.iterrows():
    all_all += all_all+str(row[4])


c=0
all_words=len(word_tokenizer.tokenize(all_all))
for word in word_tokenizer.tokenize(all_all):
    word_freq[word]+=1
    c+=1
    if c%10000==0:
        print(str(c)+' out of '+str(all_words))

# from max to min
sorted_freq = sorted(word_freq.items(), key=operator.itemgetter(1), reverse=True)

freq_file = open('../out_files/word_frequencies.csv', 'w+', encoding = 'utf-8', newline='')
fieldnames = ['word', 'frequency']
out_writer = csv.DictWriter(freq_file, fieldnames=fieldnames)
out_writer.writeheader()

for item in sorted_freq:
    word = item[0]
    frequency = item[1]
    out_writer.writerow({'word': word, 'frequency': frequency})

freq_file.close()