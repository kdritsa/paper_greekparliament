# -*- coding: utf-8 -*-

import csv
import matplotlib.pyplot as plt
import numpy as np
import powerlaw
import math
from SFAnalysis.analysis import fit


freq_file = open('word_frequencies_ranks.csv', 'r+', encoding = 'utf-8', newline='')

reader = csv.reader(freq_file, delimiter=',')
next(reader, None)  # skip the headers

freqs=[]
int_freqs=[]
ranks=[]
c=0
for line in reader:
    freqs.append(math.log(int(line[1])))
#    freqs.append(int((math.log(int(line[1])))))
    int_freqs.append(int((math.log(int(line[1])))))
    ranks.append(int(line[2]))


# Distribution Graph      
x = ranks
y = freqs
plt.plot(x, y, linewidth=2)
plt.xlabel('Ranking', fontsize=12)
plt.ylabel('Frequencies', fontsize=12)
plt.savefig('freq_rank_plot.png', dpi=200)
plt.show()


n_y = np.asarray(freqs)
#data = np.stack((n_x,n_y),axis=1) #ka8etos pinakas freq-rank

print('\nPowerlaw Package')
fit2 = powerlaw.Fit(n_y)
print('alpha: '+str(fit2.power_law.alpha))
print('sigma: '+str(fit2.power_law.sigma))
print('xmin: '+str(fit2.power_law.xmin))
R, p = fit2.loglikelihood_ratio('power_law', 'lognormal')
#R: Loglikelihood ratio of the two distributions' fit to the data. If
#greater than 0, the first distribution is preferred. If less than
#0, the second distribution is preferred.
print('R: '+str(R))
#p: Significance of R
print('p: '+str(p))
print('n_tail:'+str(fit2.n_tail))
print('goodness of fit statistic (Kolmogorov-Smirnov): '+str(fit2.power_law.KS(n_y)))


#fig2 = fit2.plot_pdf(color='b', linewidth=2)
#fit2.power_law.plot_pdf(color='g', linestyle='--', ax=fig2)
#fit2.plot_ccdf(color='r', linewidth=2, ax=fig2)


print('\nSFAnalysis')
alpha, xmin, dp, logl, gof = fit.pl(np.asarray(int_freqs))
print('alpha: '+str(alpha))
print('xmin: '+str(xmin))
#(datapoints above (and including) xmin):
print('n_tail: '+str(dp))
print('log_likelihood of fit: '+str(logl))
print('goodness of fit statistic (Kolmogorov-Smirnov): '+str(gof))

print('p-value of the returned fit (reject PL hypothesis for p<0.1): '+str(fit.plpval(np.asarray(int_freqs), alpha, xmin, gof)))


freq_file.close()
