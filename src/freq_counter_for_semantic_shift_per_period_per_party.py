# -*- coding: utf-8 -*-
import unicodedata
from collections import defaultdict
import re
from collections import Counter
import operator
import pandas as pd
import time
import datetime

print(datetime.datetime.now().time())

# \u00b7 is middle dot
# \u0387 is Greek ano teleia
punct_regex = re.compile(r"([?.!,;\u00b7\u0387])")

def str_clean(s):
   normalized = ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')
   separate_punct = re.sub(punct_regex, r" \1 ", normalized)
   collapse_spaces  = re.sub(r'\s+', " ", separate_punct)
   lc = collapse_spaces.lower()
   return lc

word_freq = defaultdict(int)

df = pd.read_csv('../out_files/tell_all_cleaned.csv') #,
print('read input file')
df = df[df['speech'].notna()]
print('cleaning speeches...')
df.speech = df.speech.apply(lambda x: str_clean(x))
print('speeches cleaned')

df.speech = df.speech.apply(lambda x: x.replace(".", " "))
df.sitting_date = pd.to_datetime(df.sitting_date, format="%d/%m/%Y")
df = df.drop(df[df.political_party == 'αντιπολιτευση'].index) #remove rows from αντιπολιτευση or βουλη
df = df.drop(df[df.political_party == 'βουλη'].index)
df = df.drop(df[df.political_party == 'εξωκοινοβουλευτικός'].index)

df = df.rename(columns={'parliamentary_period': 'period', 'political_party':'party'})

df.period = df.period.apply(lambda x: x.replace(' review 9',''))
df.period = df.period.apply(lambda x: x.replace('period ',''))
df.period = df.period.astype(int)

df.loc[(df.period==5), 'period'] = 7
df.loc[(df.period==6), 'period'] = 7
df.loc[(df.period==14), 'period'] = 15 #2012-2014
df.loc[(df.period==16), 'period'] = 17 #2015-2019

parties_selected = ['πανελληνιο σοσιαλιστικο κινημα', 'νεα δημοκρατια', 'κομμουνιστικο κομμα ελλαδας',
                   'συνασπισμος ριζοσπαστικης αριστερας', 'λαικος συνδεσμος - χρυση αυγη']

selected_parties_df = df.loc[df.party.isin(parties_selected)]


PERperiod_PERparty_df = selected_parties_df.groupby(['period','party'])[
    'speech'].apply(' '.join).reset_index()


for party in parties_selected:
    print(party)

    party_subdf = PERperiod_PERparty_df.loc[(
            PERperiod_PERparty_df.party==party)]

    for period in sorted(list(set(party_subdf.period.to_list()))):
        print(period)
        period_party_subdf = party_subdf.loc[(party_subdf.period==period)]
        period_party_subdf.speech = period_party_subdf.speech.str.lower()
        period_party_subdf.speech = period_party_subdf.speech.apply(
            lambda x: re.sub("\s\s+" , ' ', x))

        freqs = Counter()
        period_party_subdf.speech = period_party_subdf.speech.apply(lambda x:
                                                                  freqs.update(x.split()))
        print('finished counting')
        total_number = sum(freqs.values())
        print('total number of tokens:', total_number)

        freqs_df = pd.DataFrame.from_dict(freqs, orient='index',
                                          columns=['frequency'])
        freqs_df = freqs_df.reset_index()

        freqs_df = freqs_df.rename(columns={'index': 'word'})
        mask = (freqs_df['word'].str.len() > 1)
        freqs_df = freqs_df.loc[mask]
        print('Removed entries with one character.')

        freqs_df = freqs_df.sort_values('frequency').reset_index(drop=True)

        freqs_df['percentage'] = freqs_df['frequency'] / total_number

        freqs_df.to_csv(
            '../out_files/freqs_for_semantic_shift_cleaned_data_'+str(
                period)+'_'+str(party)+'_Feb2022.csv', index=False)

print(datetime.datetime.now().time())
