# -*- coding: utf-8 -*-

import pandas as pd
import csv
import jellyfish

# This script creates for each lexicon a file with the best matches for
# the 400 most common words of our corpus followed by their sentiment

def max_dist(word, first_half_word, f, lexicon):
    # Searching in the Greek sentiment lexicon with the 6 sentiments
    max_dist = 0    
    f.seek(0) #in order to iterate many times through lexicon

    for entry in lexicon:

        term = entry['term']
        first_half_term = term[0:(int(len(term)/2+1))]
        
        if first_half_word.startswith(first_half_term) or first_half_term.startswith(first_half_word):
            
            dist = jellyfish.jaro_winkler(word,term)
            
            if dist>max_dist:
                
                max_dist = dist
                maxterm=term
                maxentry = entry

    # Stricter for shorter words
    if len(word)<5:
        if max_dist>=0.97:
            return(maxterm, maxentry)
        else:
            return(None,None)
    
    elif max_dist>=0.91:
        return(maxterm, maxentry)
    
    else:
        return(None,None)


# Read the 400 most common words
words_freqs = pd.read_csv('../out_files/word_frequencies.csv')
top400 = words_freqs.loc[0:399]
  
f1 = open('out_lexicon.csv', 'r+', encoding = 'utf-8')
sent6_lexicon = csv.DictReader(f1)

# Create file with those of the top400 words with good similarity score with the 6sent lexicon
# This file is to be manually edited and renamed to top400_6sent_manually_accepted.csv
top400_6sent = open('../out_files/top400_bestmatch_6sent.csv', 'w+', encoding = 'utf-8', newline='')
# The "accept" column (default value "no") will be edited manually and declares whether we accept the match or not
fieldnames1 = ['accept','top_words', 'lexicon_terms', 'anger', 'disgust', 'fear', 'happiness', 'sadness', 'surprise']
out_writer1 = csv.DictWriter(top400_6sent, fieldnames=fieldnames1)
out_writer1.writeheader()

f2 = open('out_lexicon_pos_neg.csv', 'r+', encoding = 'utf-8')
pos_neg_sent_lexicon = csv.DictReader(f2)

# Create file with those of the top400 words with good similarity score with the pos_neg lexicon
# This file is to be manually edited and renamed to top400_pos_neg_manually_accepted.csv
top400_pos_neg = open('../out_files/top400_bestmatch_pos_neg.csv', 'w+', encoding = 'utf-8', newline='')
# The "accept" column (default value "no") will be edited manually and declares whether we accept the match or not
fieldnames2 = ['accept','top_words', 'lexicon_terms', 'positive/negative']
out_writer2 = csv.DictWriter(top400_pos_neg, fieldnames=fieldnames2)
out_writer2.writeheader()

# For each one of the most common words of our corpus
for index, row in top400.iterrows():
    word = row[0]
    freq = row[1]
    first_half_word = word[0:(int(len(word)/2+1))]

    maxterm1,maxentry1 = max_dist(word,first_half_word, f1,sent6_lexicon)
    if maxterm1!=None:    
        out_writer1.writerow({'accept':'no','top_words': word, 'lexicon_terms': maxterm1, 'anger': maxentry1['anger'], 'disgust': maxentry1['disgust'], 'fear': maxentry1['fear'], 'happiness': maxentry1['happiness'], 'sadness': maxentry1['sadness'], 'surprise': maxentry1['surprise']})

    maxterm2,maxentry2 = max_dist(word,first_half_word,f2,pos_neg_sent_lexicon)
    if maxterm2!=None:
        out_writer2.writerow({'accept':'no','top_words': word, 'lexicon_terms': maxterm2, 'positive/negative': maxentry2['positive/negative']})

        

f1.close()
f2.close()
top400_6sent.close()
top400_pos_neg.close()