## -*- coding: utf-8 -*-
#we don't remove stopwords as the sentiment lexicon includes some
import re
import csv
from cltk.tokenize.word import WordTokenizer
import jellyfish
from collections import defaultdict


def sent6_vec(text):
    
    word_vecs = []
    
    for text_word in word_tokenizer.tokenize(text):

        
        f3.seek(0) #in order to iterate many times through top words
        not_matched = True
        
        # First check in the top 400 most frequent words    
        for row in top400_6sent:
            # if word from text matches exactly with one of the top 400 most frequent words
            # and if we have manually accepted the match
            if text_word == row['top_words'] and row['accept']=='yes':
                word_vecs.append([row['anger'], row['disgust'], row['fear'], row['happiness'], row['sadness'], row['surprise']])
                not_matched = False
                break;
        
        if not_matched:
        
            first_half_word = text_word[0:(int(len(text_word)/2+1))]
            max_dist = 0
            f2.seek(0) #in order to iterate many times through lexicon
            
            for entry in lexicon_6sent:
                term = entry['term']
                first_half_term = term[0:(int(len(term)/2+1))]
                if first_half_word.startswith(first_half_term) or first_half_term.startswith(first_half_word):
                    
                    dist = jellyfish.jaro_winkler(text_word,term)
                    if dist>max_dist:
                        max_dist = dist
        
            if max_dist>=0.97:
                word_vecs.append([entry['anger'], entry['disgust'], entry['fear'], entry['happiness'], entry['sadness'], entry['surprise']])
    
    
    sum1, sum2, sum3, sum4, sum5, sum6 = 0,0,0,0,0,0
    N1, N2, N3, N4, N5, N6 = 0,0,0,0,0,0
    
    # sum of squares for each sentiment of each word
    for word_vec in word_vecs:  
        if 'N/A' not in word_vec[0]:
            sum1+= (float(word_vec[0]))**2.0
            N1+=1
        if 'N/A' not in word_vec[1]:
            sum2+= (float(word_vec[1]))**2.0
            N2+=1
        if 'N/A' not in word_vec[2]:
            sum3+= (float(word_vec[2]))**2.0
            N3+=1
        if 'N/A' not in word_vec[3]:
            sum4+= (float(word_vec[3]))**2.0
            N4+=1
        if 'N/A' not in word_vec[4]:
            sum5+= (float(word_vec[4]))**2.0
            N5+=1
        if 'N/A' not in word_vec[5]:
            sum6+= (float(word_vec[5]))**2.0
            N6+=1
    
    
    text_vec=[]
    # Square root of each sum devided by the number of words
    text_vec.append((round(((sum1/N1) ** 0.5), 2)) if N1!=0 else 0)
    text_vec.append((round(((sum2/N2) ** 0.5), 2)) if N1!=0 else 0)
    text_vec.append((round(((sum3/N3) ** 0.5), 2)) if N1!=0 else 0)
    text_vec.append((round(((sum4/N4) ** 0.5), 2)) if N1!=0 else 0)
    text_vec.append((round(((sum5/N5) ** 0.5), 2)) if N1!=0 else 0)
    text_vec.append((round(((sum6/N6) ** 0.5), 2)) if N1!=0 else 0)
    
    return(text_vec)


def pos_neg_counter(text):
    
    word_ratings = []
    
    for text_word in word_tokenizer.tokenize(text):

        
        f5.seek(0) #in order to iterate many times through top words
        not_matched = True
        
        # First check in the top 400 most frequent words    
        for row in top400_pos_neg:
            # if word from text matches exactly with one of the top 400 most frequent words
            # and if we have manually accepted the match
            if text_word == row['top_words'] and row['accept']=='yes':
                word_ratings.append([row['positive/negative']])
                not_matched = False
                break;
        
        if not_matched:
        
            first_half_word = text_word[0:(int(len(text_word)/2+1))]
            max_dist = 0
            f4.seek(0) #in order to iterate many times through lexicon
            
            for entry in lexicon_pos_neg:
                term = entry['term']
                first_half_term = term[0:(int(len(term)/2+1))]
                if first_half_word.startswith(first_half_term) or first_half_term.startswith(first_half_word):
                    
                    dist = jellyfish.jaro_winkler(text_word,term)
                    if dist>max_dist:
                        max_dist = dist
        
            if max_dist>=0.97:
                word_ratings.append([entry['positive/negative']])
    
    
    pos_sum = 0
    neg_sum = 0
    
    # sum of squares for each sentiment of each word
    for rating in word_ratings:  
        if rating=='+':
            pos_sum+=1
        elif rating=='-':
            neg_sum+=1
            
    return(pos_sum, neg_sum)


word_tokenizer = WordTokenizer('greek')

# Read the text we are interested in finding the sentiment of
f1 = open('sentiment_test.txt', 'r+', encoding='utf-8') #encoding for Greek
tellall = ((f1.read()).lower()).translate(str.maketrans('άέόώήίϊΐύϋΰ','αεοωηιιιυυυ'))

f2 = open('out_lexicon_6sent.csv', 'r+', encoding = 'utf-8')
lexicon_6sent = csv.DictReader(f2)

f3 = open('top400_6sent_manually_accepted.csv', 'r+', encoding = 'utf-8')
top400_6sent = csv.DictReader(f3)

f4 = open('out_lexicon_pos_neg.csv', 'r+', encoding = 'utf-8')
lexicon_pos_neg = csv.DictReader(f4)

f5 = open('top400_pos_neg_manually_accepted.csv', 'r+', encoding = 'utf-8')
top400_pos_neg = csv.DictReader(f5)

f6 = open('sentiment_analyzer_output.txt', 'a+', encoding = 'utf-8')

all_all=''

all_eras_party = defaultdict(str)
all_parties_era = defaultdict(str)

era1_parties = defaultdict(str)
era2_parties = defaultdict(str)
era3_parties = defaultdict(str)
era4_parties = defaultdict(str)
era5_parties = defaultdict(str)
era6_parties = defaultdict(str)

counter=0
x = len(tellall.splitlines())
for line in tellall.splitlines():
    year = (line.split(',')[1]).split('-')[0]
    party = line.split(',')[2]
    speech = (line.split('speech:')[1]).rstrip('\n') #remove trailing newlines
    speech = re.sub("[.,!;():?«»]",' ', speech) #remove some punctuation, not accents

    counter+=1
    if counter%5000==0:
        print(str(counter)+' of '+str(x))
        
    all_all+=speech    
    all_eras_party[party]+=speech
   
    if int(year)<1994:
        era1_parties[party]+=speech
        all_parties_era['era1']+=speech
    elif int(year)<1999:
        era2_parties[party]+=speech
        all_parties_era['era2']+=speech
    elif int(year)<2004:
        era3_parties[party]+=speech
        all_parties_era['era3']+=speech
    elif int(year)<2009:
        era4_parties[party]+=speech
        all_parties_era['era4']+=speech
    elif int(year)<2014:
        era5_parties[party]+=speech
        all_parties_era['era5']+=speech
    else:
        era6_parties[party]+=speech
        all_parties_era['era6']+=speech

f6.write('*Generic Parliament Sentiment (all times - all parties)*\n')
f6.write('6 Sentiments Lexicon vector:'+str(sent6_vec(all_all)))
f6.write('Positive/Negative Lexicon sums:'+str(pos_neg_counter(all_all)))

f6.write('\n\n*Sentiment in each of the 6 eras:*\n')
for era, speech in all_parties_era.items():
    f6.write('\n\n'+era+' - 6 Sentiments Lexicon vector: '+str(sent6_vec(speech)))
    f6.write('\n'+era+' - Positive/Negative Lexicon sums: '+str(pos_neg_counter(speech)))


#Metrics concerning parties at all times
f6.write('\n\n*Sentiment of each political party throughout all times*\n')
for party, speech in all_eras_party.items():
    f6.write('\n\n'+party+' - 6 Sentiments Lexicon vector: '+str(sent6_vec(speech)))
    f6.write('\n\n'+party+' - Positive/Negative Lexicon sums: '+str(pos_neg_counter(speech)))


#Metrics concerning each party in each era
f6.write('\n\n*Sentiment of each political party at each era*')
f6.write('\n\nEra 1989-1993: ')
for party, speech in era1_parties.items():
    f6.write('\n\n'+party+' - 6 Sentiments Lexicon vector: '+str(sent6_vec(speech)))
    f6.write('\n'+party+' - Positive/Negative Lexicon sums: '+str(pos_neg_counter(speech)))

f6.write('\n\nEra 1994-1998: ')
for party, speech in era2_parties.items():
    f6.write('\n\n'+party+' - 6 Sentiments Lexicon vector: '+str(sent6_vec(speech)))
    f6.write('\n'+party+' - Positive/Negative Lexicon sums: '+str(pos_neg_counter(speech)))

f6.write('\n\nEra 1999-2003: ')
for party, speech in era3_parties.items():
    f6.write('\n\n'+party+' - 6 Sentiments Lexicon vector: '+str(sent6_vec(speech)))
    f6.write('\n'+party+' - Positive/Negative Lexicon sums: '+str(pos_neg_counter(speech)))

f6.write('\n\nEera 2004-2008: ')
for party, speech in era4_parties.items():
    f6.write('\n\n'+party+' - 6 Sentiments Lexicon vector: '+str(sent6_vec(speech)))
    f6.write('\n'+party+' - Positive/Negative Lexicon sums: '+str(pos_neg_counter(speech)))

f6.write('\n\nEra 2009-2013: ')
for party, speech in era5_parties.items():
    f6.write('\n\n'+party+' - 6 Sentiments Lexicon vector: '+str(sent6_vec(speech)))
    f6.write('\n'+party+' - Positive/Negative Lexicon sums: '+str(pos_neg_counter(speech)))

f6.write('\n\nEra 2014-2017: ')
for party, speech in era6_parties.items():
    f6.write('\n\n'+party+' - 6 Sentiments Lexicon vector: '+str(sent6_vec(speech)))
    f6.write('\n'+party+' - Positive/Negative Lexicon sums: '+str(pos_neg_counter(speech)))

f1.close()
f2.close()
f3.close()
f4.close()
f5.close()
f6.close()