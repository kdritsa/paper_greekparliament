# # -*- coding: utf-8 -*-
import pandas as pd
import numpy as np

df = pd.read_csv('../out_files/tell_all_more_inline_speakers_filled.csv')

# Discard column of speaker info
df = df.drop(['speaker_info', 'parliamentary_period', 'parliamentary_session'], axis=1)

# Replace NaN with empty strings, in order to easily join strings
# df_joined = df.replace(np.nan, '')
s=''
for index, row in df.iterrows():
    if row['speech'] == '':
        print(row)
        print('---------------')
    elif row['speech'] == 'nan':
        print(row)
        print('---------------')

    # try:
    #     s += row['speech']
    # except:
    #     print(row)

# # Join speech column values, group them by speaker, date, sitting, party
# df_joined = df.groupby(['member_name', 'sitting_date', 'parliamentary_sitting', 'political_party'])['speech'].apply(lambda x: ' '.join(x))
#
# df_joined.to_csv('../out_files/tell_all_speaker_per_sitting.csv', encoding='utf-8')
