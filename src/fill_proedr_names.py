import pandas as pd
import numpy as np
import math

all = open('../out_files/tell_all_more_inline_speakers.csv', 'r+', encoding='utf-8', newline = '')

df_all = pd.read_csv(all, encoding='utf-8')

df = df_all[['member_name', 'sitting_date', 'parliamentary_sitting', 'speaker_info']]

# create a new df with proedreuontes names and info only, as a prototype and reference
df_proedr = df.loc[((df['speaker_info'] == 'προεδρευων') | (df['speaker_info'].str.contains('προεδρος')))]

# group the new df by date and sitting number. this joins the not-nan names of proedreuontes with ' / '
df_grouped_proedr = df_proedr.groupby(['sitting_date', 'parliamentary_sitting', 'speaker_info'])['member_name'].apply(lambda x: ' / '.join(set(x.dropna()))).reset_index()

# fill emptied cells with NaN value
df_grouped_proedr = df_grouped_proedr.replace('', np.nan)

# for each of the rows of the initial file that does not have a proedreuon name
# search in the new df of proedreuontes for the name and replace NaN with the actual name
for index, row in df_all.iterrows():
    if pd.isnull(row['member_name']):
        if any(word in row['speaker_info'] for word in ['προεδρευων','προεδρος']):
            data = df_grouped_proedr.loc[(df_grouped_proedr['sitting_date'] == row['sitting_date']) & (df_grouped_proedr['parliamentary_sitting'] == row['parliamentary_sitting'])].reset_index()
            proedr_name = data['member_name'][0] #row 0 column member_name
            df_all.loc[index, 'member_name'] = proedr_name

for index, row in df_all.iterrows():
    if len(row['member_name'].split('/'))>1:
        print(row['member_name'])

df_all.to_csv('../out_files/tell_all_more_inline_speakers_filled.csv', index=False, na_rep=np.nan)
print('Done')