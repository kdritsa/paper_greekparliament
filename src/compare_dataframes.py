import pandas as pd

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("-f1", "--file1")
parser.add_argument("-f2", "--file2")

args = vars(parser.parse_args())
file1 = args['file1']
file2 = args['file2']

df_1 = pd.read_csv(file1) 
df_2 = pd.read_csv(file2)

if df_1.equals(df_2):
	print('Same dataframes')
else:
	print('Different dataframes')


