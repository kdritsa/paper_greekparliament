#!/bin/bash

iterations=10
vector_size=300
no_rows='all'

PYTHONHASHSEED=0 python goldberg_stability_1990s_2010s.py --run=0 --iterations=$iterations --size=$vector_size --rows=$no_rows
echo 'finished run 0'
PYTHONHASHSEED=0 python goldberg_stability_1990s_2010s.py --run=1 --iterations=$iterations --size=$vector_size --rows=$no_rows
echo 'finished run 1'
python compare_dataframes.py -f1='../out_files/stability_goldberg_run0_iterations'$iterations'_size'$vector_size'_rows'$no_rows'.csv' -f2='../out_files/stability_goldberg_run1_iterations'$iterations'_size'$vector_size'_rows'$no_rows'.csv'
