import re

#Cleaning and formatting speakers data

def text_formatting(text):
    text = re.sub("[():'’`΄‘]",' ', text)#-–
    text = re.sub('\t+' , ' ', text) #replace one or more tabs with one space
    text = text.lstrip() #remove leading spaces
    text = text.rstrip() #remove trailing spaces
    text = re.sub('\s\s+' , ' ', text) #replace more than one spaces with one space
    text = re.sub('\s*(-|–)\s*' , '-', text) #fix dashes
    text = text.lower()
    text = text.translate(str.maketrans('άέόώήίϊΐiύϋΰ','αεοωηιιιιυυυ')) #remove accents
    text = text.translate(str.maketrans('akebyolruxtvhmnz','ακεβυολρυχτνημνζ')) #convert english characters to greek
    return(text) 
    
    
lines_before=0

#Open existing file
f1 = open('original_speakers_data.txt', 'r+', encoding='utf-8') #encoding for Greek
original_speakers_data = f1.read()

nickname_in_parenthesis = re.compile(r"(\([Α-ΩΆ-Ώ]+\))+") #(ΠΑΝΟΣ)
text_in_parenthesis = re.compile(r"(\(.*?\)){1}") #(Υπουργός Εσωτερικών)
proedreuon_regex = re.compile(r"(^((Π+Ρ(Ο|Ό)+(Ε|Έ))|(Ρ(Ο|Ό)+(Ε|Έ)Δ)|(ΠΡ(Ε|Έ)(Ο|Ό))|(ΠΡ(Ο|Ό)Δ)|(Η ΠΡ(Ο|Ό)(Ε|Έ)ΔΡ)|(ΠΡ(Ε|Έ)Δ)))")
general_member_regex = re.compile(r"((Β(Ο|Ό)(Υ|Ύ)(Ε|Έ)Λ)|(Β(Ο|Ό)(Υ|Ύ)Λ(Ε|Έ)(Υ|Ύ)?Τ[^(Α|Ά)]))")

left_parenthesis_regex = re.compile(r"\(")
right_parenthesis_regex = re.compile(r"\)")
incomplete_nickname_parenthesis = re.compile(r"\([Α-ΩΆ-Ώ]{3,}\s")

#Create updated speakers file, keep unique names and delete parenthesis text
f2 = open('speakers_data.txt','w+', encoding='utf-8')

for line in original_speakers_data.splitlines():
    
    lines_before+=1
    
    speaker = line.split(',')[0]
    year = line.split(',')[1]
    speaker_info=''
    nickname=''

    #in case the speaker name is like "ΠΡΟΕΔΡΕΥΩΝ (Παναγιώτης Ν. Κρητικός):"
    if proedreuon_regex.match(speaker):
#    if speaker.startswith('ΠΡΟΕ' or 'ΠΡΕΟ' or 'ΠΡΟΔ' or 'Η ΠΡΟΕΔΡ'):
        
        segments = speaker.split('(')
        speaker = ''.join(segments[1:])
    
    if speaker.startswith('ΜΑΡΤΥΣ'):
        speaker = speaker.replace('ΜΑΡΤΥΣ','')
        speaker = re.sub("[()]",'', speaker)
        if len(speaker)<3: #for cases where the name of the person is not mentioned
            speaker=''
    
    # for example ΠΟΛΛΟΙ ΒΟΥΛΕΥΤΕΣ (από την πτέρυγα του ΠΑ.ΣΟ.Κ.):,2006
    if general_member_regex.match(speaker):
        #temporarily exclude this info
        #cases: πασοκ, δημοκρατια, συνασπισμου | αριστερας | προοδου, λαος, συριζα
        #exclude εφηβοι?
        #case: ΕΝΑΣ ΒΟΥΛΕΥΤΗΣ (Του Συνασπισμού της Αριστέρας και της Προόδου):,1990
        speaker=''
    
    if speaker!='':
        
        #For example ΦΩΤΕΙΝΗ (ΦΩΦΗ ΓΕΝΝΗΜΑΤΑ (Πρόεδρος της Δημοκρατικής Συμπαράταξης ΠΑΣΟΚ - ΔΗΜΑΡ):,2017
        lefts=0
        rights=0
        if left_parenthesis_regex.search(speaker):
            lefts = len(re.findall(left_parenthesis_regex,speaker))
        if right_parenthesis_regex.search(speaker):
            rights = len(re.findall(right_parenthesis_regex,speaker))
        if (lefts-rights)>0:
            if incomplete_nickname_parenthesis.search(speaker):
                # Keep separately the nickname of the speaker
                nickname = (incomplete_nickname_parenthesis.search(speaker)).group()
                nickname = text_formatting(nickname)
                speaker = re.sub(incomplete_nickname_parenthesis, '', speaker) #remove nickname        
#        
        # Keep separately the nickname of the speaker
        if nickname_in_parenthesis.search(speaker):
            nickname = (nickname_in_parenthesis.search(speaker)).group()
            nickname = text_formatting(nickname)
            speaker = re.sub(nickname_in_parenthesis, '', speaker) #remove nickname        
            
        #keep separately the explanatory parenthesis text of the speaker
        if text_in_parenthesis.search(speaker):
            speaker_info = (text_in_parenthesis.search(speaker)).group()
            speaker_info = text_formatting(speaker_info)
            speaker_info = speaker_info.replace('υφυπ.',' υφυπουργος ')
            speaker_info = speaker_info.replace('υπ.',' υπουργος ')
            speaker_info = speaker_info.replace('&',' και ')
            speaker_info = re.sub('\s\s+' , ' ', speaker_info) #replace more than one spaces with one space
            speaker_info = speaker_info.lstrip() #remove leading spaces
            speaker_info = speaker_info.rstrip() #remove trailing spaces
            speaker = re.sub(text_in_parenthesis, '', speaker) #remove (text in parenthesis)        
                  
        speaker = text_formatting(speaker)
           
        if len(nickname)>0:
            f2.write(speaker+' ('+nickname+'),'+year+','+speaker_info+'\n')
        else:
            f2.write(speaker+','+year+','+speaker_info+'\n')
    else:
        pass   
    
f1.close()

print('Lines before: '+str(lines_before))

f2.seek(0)
unique_lines = set(f2.readlines())
print('Lines after: '+str(len(unique_lines)))

f2.seek(0)
f2.writelines(unique_lines)
f2.truncate()
f2.close()