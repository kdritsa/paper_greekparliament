import math
import re
from nltk.tokenize import sent_tokenize, word_tokenize
from textstat.textstat import textstat
import invoice

empty_sentence = re.compile(r"(^\s*[α-ωά-ώΑ-ΩΆ-Ώ]{0,2}\s*[.;?]*$)")

text = (open('60-english-sentences.txt', 'r+', encoding='utf-8')).read() #encoding for Greek
sample_text = []

text = text.replace('!','.') #replace '!' with '.'

print('TEXTSTAT')
print('smog: '+str(textstat.smog_index(text)))
#print('words: '+str(textstat.lexicon_count(t)))
#print('sentences: '+str(textstat.sentence_count(t)))
#print('syllables: '+str(textstat.syllable_count(t)))
print('polysyllables: '+str(textstat.polysyllabcount(text)))

print('\nOUR SCRIPT')
sent_text = sent_tokenize(text) 

# Keep non empty sentences
sent_text = [sent for sent in sent_text if not empty_sentence.search(sent)]



if len(sent_text)>=60:
    
    part1 = sent_text[0:(int(len(sent_text)/3))]
    part2 = sent_text[((int(len(sent_text)/3))):(int(2/3*len(sent_text)))]
    part3 = sent_text[((int(2/3*len(sent_text)))):]

    first20 = part1[(int(len(part1)/2)-10):(int(len(part1)/2)+10)]

    middle20 = part2[(int(len(part2)/2)-10):(int(len(part2)/2)+10)]
#    print(middle10)
    last20 = part3[(int(len(part3)/2)-10):(int(len(part3)/2)+10)]
#    print(last10)
    
    sample_text = first20 + middle20 + last20

    sample_text = [(' '.join(invoice.num_to_text(int(word)) if word.isdigit() else word for word in word_tokenize(sent))) for sent in sample_text]

    sent_num = len(sample_text)
    
    polysyllables=0
    all_words=0
    all_syll=0
    
    for sent in sample_text:
        for word in word_tokenize(sent):
            all_words+=1
            syllables = textstat.syllable_count(word)
            all_syll+=syllables
            if syllables>2:
                polysyllables+=1
 
    
    grade = 1.0430*math.sqrt(polysyllables*30/sent_num)+3.1291
    grade = int(grade * 100.0) / float(100.0) #up to second decimal
    print('smog: '+str(grade))
#    print('words: '+str(all_words))
#    print('syllables: '+str(all_syll))
    print('polysyllables: '+str(polysyllables))

else:
    print('Please, provide text with more than 60 sentences.')