# from cltk.corpus.utils.importer import CorpusImporter

# corpus_importer = CorpusImporter('greek')

from cltk.tokenize.sentence import TokenizeSentence
from greek_accentuation.syllabify import syllabify

from nltk.tokenize.punkt import PunktLanguageVars

tokenizer = TokenizeSentence('greek')

p = PunktLanguageVars()

# http://news.in.gr/science-technology/article/?aid=1500197131
untokenized_text = """ 
Ο μεγάλος αστεροειδής που πιστεύεται ότι έπεσε στη Γη πριν από περίπου
66 εκατομμύρια χρόνια, με συνέπεια την εξαφάνιση των δεινοσαύρων,
πιθανώς προκάλεσε σειρά κατακλυσμικών ηφαιστειακών εκρήξεων σε ξηρά
και θάλασσα, οι οποίες ολοκλήρωσαν το καταστροφικό έργο της αρχικής
πρόσκρουσης.

Το σενάριο αυτό ενισχύεται από νέες ενδείξεις που βρήκαν Aμερικανοί
επιστήμονες στο βυθό του Ειρηνικού και του Ινδικού Ωκεανού.

Όταν ο διαμέτρου δέκα χιλιομέτρων αστεροειδής συνετρίβη στην περιοχή
του σημερινού Γιουκατάν του Μεξικού, δημιουργώντας τον διασημότερο
κρατήρια του κόσμου, τον Τσιξουλούμπ, οι περιβαλλοντικές επιπτώσεις
(πυρκαγιές, καυτοί άνεμοι, νέφη σωματιδίων που σκίασαν τον ουρανό,
απότομη πτώση θερμοκρασίας, τρομεροί σεισμοί έως 100 φορές μεγαλύτεροι
από τους σημερινούς κ.α.) δημιούργησαν μια κόλαση επί της γης.
"""

tokenized_text = tokenizer.tokenize_sentences(untokenized_text)
print(len(tokenized_text))
for sentence in tokenized_text:
    print('===')
    tokens = p.word_tokenize(sentence)
    for token in tokens:
        print(syllabify(token))
    
