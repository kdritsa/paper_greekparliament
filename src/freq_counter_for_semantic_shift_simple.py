import unicodedata
from collections import defaultdict
import re
from collections import Counter
import operator
import pandas as pd
import time

# \u00b7 is middle dot
# \u0387 is Greek ano teleia
punct_regex = re.compile(r"([?.!,;\u00b7\u0387])")

def str_clean(s):
   normalized = ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')
   separate_punct = re.sub(punct_regex, r" \1 ", normalized)
   collapse_spaces  = re.sub(r'\s+', " ", separate_punct)
   lc = collapse_spaces.lower()
   return lc

word_freq = defaultdict(int)

df = pd.read_csv('../out_files/tell_all_cleaned.csv')
print('read input file')
df = df[df['speech'].notna()]
print('cleaning speeches...')
df.speech = df.speech.apply(lambda x: str_clean(x))
print('speeches cleaned')

freqs = Counter()
df.speech.apply(lambda x: freqs.update(x.split()))
print('finished counting')
total_number = sum(freqs.values())
print('total number of tokens:', total_number)


freqs_df = pd.DataFrame.from_dict(freqs, orient='index', columns = ['frequency'])
freqs_df = freqs_df.reset_index()

freqs_df = freqs_df.rename(columns={'index': 'word'})
mask = (freqs_df['word'].str.len() > 1)
freqs_df = freqs_df.loc[mask]
print('Removed entries with one character.')

freqs_df = freqs_df.sort_values('frequency').reset_index(drop=True)

freqs_df['percentage'] = freqs_df['frequency']/total_number

freqs_df.to_csv('../out_files/freqs_for_semantic_shift_cleaned_data.csv', index=False)
