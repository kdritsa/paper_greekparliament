# -*- coding: utf-8 -*-

import csv

# Write words from both files in one sentiment lexicon

f1 = open('positive_words_el.txt', 'r+', encoding = 'utf-8')
pos = [word.strip() for word in f1.readlines()]
pos_terms = [word.translate(str.maketrans('άέόώήίϊΐύϋΰ','αεοωηιιιυυυ')) for word in pos] #remove accents

f2 = open('negative_words_el.txt', 'r+', encoding = 'utf-8')
neg = [word.strip() for word in f2.readlines()]
neg_terms = [word.translate(str.maketrans('άέόώήίϊΐύϋΰ','αεοωηιιιυυυ')) for word in neg] #remove accents

out_lexicon = open('out_lexicon_pos_neg.csv', 'w+', encoding = 'utf-8', newline='')
fieldnames = ['term', 'positive/negative']
out_writer = csv.DictWriter(out_lexicon, fieldnames=fieldnames)
out_writer.writeheader()

for term in pos_terms:
    out_writer.writerow({'term': term, 'positive/negative': '+'})

for term in neg_terms:
    out_writer.writerow({'term': term, 'positive/negative': '-'})
    
f1.close()
f2.close()