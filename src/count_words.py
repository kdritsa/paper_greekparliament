# -*- coding: utf-8 -*-
import csv
from collections import defaultdict
import re
from cltk.tokenize.word import WordTokenizer


word_tokenizer = WordTokenizer('greek')

all_all=0

all_eras_party = defaultdict(int)
all_parties_era = defaultdict(int)

era1_parties = defaultdict(int)
era2_parties = defaultdict(int)
era3_parties = defaultdict(int)
era4_parties = defaultdict(int)
era5_parties = defaultdict(int)
era6_parties = defaultdict(int)

with open('tell_all_speaker_per_sitting.csv', 'r', encoding = 'utf-8', newline='') as csvfile:
    csv_reader = csv.reader(csvfile)
    
    for i, row in enumerate(csv_reader):
        if i % 1000 == 0:
            print(i)
        speech = row[4]
        speech = (speech.lower()).translate(str.maketrans('άέόώήίϊΐύϋΰ',
                                                          'αεοωηιιιυυυ'))
        speech = re.sub("[().,;?!:«»“”]",' ', speech)
        speech = re.sub('\s+' , ' ', speech) 

        year = row[1].split('-')[0]
        party = row[3]
        
        speech_sum = len(word_tokenizer.tokenize(speech))
        
        all_all+=speech_sum
        
        all_eras_party[party]+=speech_sum
       
        if int(year)<1994:
            era1_parties[party]+=speech_sum
            all_parties_era['era1']+=speech_sum
        elif int(year)<1999:
            era2_parties[party]+=speech_sum
            all_parties_era['era2']+=speech_sum
        elif int(year)<2004:
            era3_parties[party]+=speech_sum
            all_parties_era['era3']+=speech_sum
        elif int(year)<2009:
            era4_parties[party]+=speech_sum
            all_parties_era['era4']+=speech_sum
        elif int(year)<2014:
            era5_parties[party]+=speech_sum
            all_parties_era['era5']+=speech_sum
        else:
            era6_parties[party]+=speech_sum
            all_parties_era['era6']+=speech_sum
        

f=open('count_words_results.txt', 'w+', encoding='utf-8')

print('all_all')
print(all_all)
print('all_eras_party')
print(all_eras_party)
print('all_parties_era')
print(all_parties_era)
print('era1_parties')
print(era1_parties)
print('era2_parties')
print(era2_parties)
print('era3_parties')
print(era3_parties)
print('era4_parties')
print(era4_parties)
print('era5_parties')
print(era5_parties)
print('era6_parties')
print(era6_parties)

f.write('all_all')
f.write(str(all_all))
f.write('all_eras_party')
f.write(str(all_eras_party))
f.write('all_parties_era')
f.write(str(all_parties_era))
f.write('era1_parties')
f.write(str(era1_parties))
f.write('era2_parties')
f.write(str(era2_parties))
f.write('era3_parties')
f.write(str(era3_parties))
f.write('era4_parties')
f.write(str(era4_parties))
f.write('era5_parties')
f.write(str(era5_parties))
f.write('era6_parties')
f.write(str(era6_parties))