# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import operator

fig1 = plt.figure(figsize=(9,6))
plt.title('Percentage of positive/negative words through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]
pos_per_era = [95531*100/3365248, 66528*100/2278285, 97086*100/3205234, 109293*100/3260685, 107514*100/2907826, 80466*100/2367721]
neg_per_era = [67808*100/3365248, 43438*100/2278285, 63575*100/3205234, 71888*100/3260685, 63462*100/2907826, 46310*100/2367721]

line1, = plt.plot(time,pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=556418*100/17384999, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=356481*100/17384999, linestyle='dashdot', color='#FF0000', alpha=0.5)
plt.scatter(time,pos_per_era, c='g')
plt.scatter(time,neg_per_era, c='r')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig1.set_size_inches(14, 8)
fig1.savefig('../images/pos_neg_sentiments_through_time.png', dpi=200, bbox_inches='tight')


#************************************************
fig2 = plt.figure(figsize=(9,6))
plt.title('ND - Average percentage of positive/negative words through timee', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]

nd_pos_per_era = [50980*100/1812779, 16091*100/549089, 25689*100/872136, 58513*100/1762046, 26732*100/723946, 18859*100/581212]
nd_neg_per_era = [33788*100/1812779, 11867*100/549089, 19746*100/872136, 36245*100/1762046, 15600*100/723946, 11385*100/581212]

line1, = plt.plot(time,nd_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,nd_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=196864*100/6301208, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=128631*100/6301208, linestyle='dashdot', color='#FF0000', alpha=0.5)


plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig2.set_size_inches(14, 8)
fig2.savefig('../images/nd_pos_neg_through_time.png', dpi=200, bbox_inches='tight')

#************************************************
fig3 = plt.figure(figsize=(9,6))
plt.title('KKE - Average percentage of positive/negative words through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]

kke_pos_per_era = [1328*100/47101, 3319*100/121232, 5724*100/197598, 7207*100/216870, 7644*100/205297, 4905*100/170491]
kke_neg_per_era = [1021*100/47101, 2627*100/121232, 4076*100/197598, 4914*100/216870, 4594*100/205297, 3082*100/170491]

line1, = plt.plot(time,kke_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,kke_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=30127*100/958589, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=20314*100/958589, linestyle='dashdot', color='#FF0000', alpha=0.5)

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig3.set_size_inches(14, 8)
fig3.savefig('../images/kke_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************

fig4 = plt.figure(figsize=(9,6))
plt.title('PASOK - Average percentage of positive/negative words through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]

pasok_pos_per_era = [33076*100/1163932, 40170*100/1366479, 60702*100/1961060, 36505*100/1081224, 38458*100/1053192, 4329*100/125285]
pasok_neg_per_era = [25113*100/1163932, 23595*100/1366479, 35658*100/1961060, 25593*100/1081224, 21888*100/1053192, 2583*100/125285]

line1, = plt.plot(time,pasok_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,pasok_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=213240*100/6751172, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=134430*100/6751172, linestyle='dashdot', color='#FF0000', alpha=0.5)


plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig4.set_size_inches(14, 8)
fig4.savefig('../images/pasok_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************

fig5 = plt.figure(figsize=(9,6))
plt.title('SYRIZA - Average percentage of positive/negative words through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]

syriza_pos_per_era = [None, None, None, 4055*100/122140, 12129*100/378507, 29275*100/874309]
syriza_neg_per_era = [None, None, None, 3192*100/122140, 8766*100/378507, 16843*100/874309]

line1, = plt.plot(time,syriza_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,syriza_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=45459*100/1374956, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=28801*100/1374956, linestyle='dashdot', color='#FF0000', alpha=0.5)

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig5.set_size_inches(14, 8)
fig5.savefig('../images/syriza_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig6 = plt.figure(figsize=(9,6))
plt.title('DIMAR - Average percentage of positive/negative words through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]

dimar_pos_per_era = [None, None, None, None, 1999*100/41313, 958*100/18055]
dimar_neg_per_era = [None, None, None, None, 980*100/41313, 412*100/18055]

line1, = plt.plot(time,dimar_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,dimar_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=2957*100/59368, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=1392*100/59368, linestyle='dashdot', color='#FF0000', alpha=0.5)

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig6.set_size_inches(14, 8)
fig6.savefig('../images/dimar_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig7 = plt.figure(figsize=(9,6))
plt.title('ANEL (Panos Kammenos) - Average percentage of positive/negative words through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]

anel1_pos_per_era = [None, None, None, None, 2874*100/84364, 1796*100/53684]
anel1_neg_per_era = [None, None, None, None, 1758*100/84364, 1013*100/53684]

line1, = plt.plot(time,anel1_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,anel1_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=4670*100/138048, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=2771*100/138048, linestyle='dashdot', color='#FF0000', alpha=0.5)

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig7.set_size_inches(14, 8)
fig7.savefig('../images/anel1_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig8 = plt.figure(figsize=(9,6))
plt.title('LAOS - Average percentage of positive/negative words through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]

laos_pos_per_era = [None, None, None, 2495*100/63573, 9023*100/207655, None]
laos_neg_per_era = [None, None, None, 1562*100/63573, 4733*100/207655, None]

line1, = plt.plot(time,laos_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,laos_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=11518*100/271228, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=6295*100/271228, linestyle='dashdot', color='#FF0000', alpha=0.5)

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig8.set_size_inches(14, 8)
fig8.savefig('../images/laos_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig9 = plt.figure(figsize=(9,6))
plt.title('Golden Dawn - Average percentage of positive/negative words through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]

gd_pos_per_era = [None, None, None, None, 1582*100/42714, 2529*100/69162]
gd_neg_per_era = [None, None, None, None, 1001*100/42714, 1509*100/69162]

line1, = plt.plot(time,gd_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,gd_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=4111*100/111876, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=2510*100/111876, linestyle='dashdot', color='#FF0000', alpha=0.5)


plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig9.set_size_inches(14, 8)
fig9.savefig('../images/gd_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig10 = plt.figure(figsize=(9,6))
plt.title('DIKKI - Average percentage of positive/negative words through time', fontsize=15)


time = [1991, 1996, 2001, 2006, 2011, 2016]

dikki_pos_per_era = [None, 3022*100/102943, 1786*100/68246, None, None, None]
dikki_neg_per_era = [None, 2238*100/102943, 1437*100/68246, None, None, None]

line1, = plt.plot(time,dikki_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,dikki_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=4808*100/171189, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=3675*100/171189, linestyle='dashdot', color='#FF0000', alpha=0.5)

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig10.set_size_inches(14, 8)
fig10.savefig('../images/dikki_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig11 = plt.figure(figsize=(9,6))
plt.title('Political Spring - Average percentage of positive/negative words through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]

ps_pos_per_era = [179*100/6587, 1724*100/67254, None, None, None, None]
ps_neg_per_era = [148*100/6587, 1290*100/67254, None, None, None, None]

line1, = plt.plot(time,ps_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,ps_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=1903*100/73841, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=1438*100/73841, linestyle='dashdot', color='#FF0000', alpha=0.5)


plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig11.set_size_inches(14, 8)
fig11.savefig('../images/ps_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig12 = plt.figure(figsize=(9,6))
plt.title('Independent Members - Average percentage of positive/negative words through time', fontsize=15)


time = [1991, 1996, 2001, 2006, 2011, 2016]

im_pos_per_era = [1746*100/64074, None, 90*100/2566, 344*100/9624, 6652*100/159212, 1808*100/49756]
im_neg_per_era = [1436*100/64074, None, 60*100/2566, 237*100/9624, 3931*100/159212, 1086*100/49756]

line1, = plt.plot(time,im_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,im_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=10640*100/285232, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=6750*100/285232, linestyle='dashdot', color='#FF0000', alpha=0.5)

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig12.set_size_inches(14, 8)
fig12.savefig('../images/im_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig13 = plt.figure(figsize=(9,6))
plt.title('SYN - Average percentage of positive/negative words through time', fontsize=15)


time = [1991, 1996, 2001, 2006, 2011, 2016]

syn_pos_per_era = [7580*100/249300, 2202*100/71285, 3095*100/103625, 174*100/5208, None, None]
syn_neg_per_era = [5781*100/249300, 1821*100/71285, 2598*100/103625, 145*100/5208, None, None]

line1, = plt.plot(time,syn_pos_per_era, '#4EC247', label='Positive')
line2, = plt.plot(time,syn_neg_per_era, '#FF0000', label='Negative')

plt.axhline(y=13051*100/429418, linestyle='dashdot', color='#4EC247', alpha=0.5)
plt.axhline(y=10345*100/429418, linestyle='dashdot', color='#FF0000', alpha=0.5)

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('percentage of positive/negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig13.set_size_inches(14, 8)
fig13.savefig('../images/syn_pos_neg_through_time.png', dpi=200, bbox_inches='tight')
#************************************************

#Average positive per political party
fig14 = plt.figure(figsize=(9,6))

average_pos_per_party = {'ND':196864*100/6301208, 
                          'PASOK':213240*100/6751172, 
                          'Alternative Ecologists':326*100/11929,
                          'SYN':13051*100/429418,
                          'Independent members':10640*100/285232,
                          'Political Spring':1903*100/73841,
                          'KKE':30127*100/958589,
                          'DIKKI':4808*100/171189,
                          'LAOS':11518*100/271228,
                          'SYRIZA':45459*100/1374956,
                          'Golden Dawn':4111*100/111876,
                          'DIMAR':2957*100/59368,
                          'ANEL - Panos Kammenos':4670*100/138048,
                          'KKE Interior':171*100/5882,
                          'DIANA':136*100/3445,
                          'Independent Democratic MPs':685*100/16622,
                          'The River':3295*100/79108,
                          'DISI':7696*100/216030,
                          'Union of Centrists':1546*100/43994,
                          'ANEL - NPD Alliance':2581*100/62234,
                          'LAE':622*100/19319
                          }

sorted_average = dict(sorted(average_pos_per_party.items(), key=operator.itemgetter(1)))

labels = [key for key,value in sorted_average.items()]

pos_scores = [value for key,value in sorted_average.items()]

cm = plt.get_cmap('Greens')
#plt.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

index = np.arange(len(labels))
barlist = plt.bar(index, pos_scores, width=0.6)
plt.xlabel('Political parties', fontsize=12)
plt.ylabel('Average percentage of positive words', fontsize=12)
plt.ylim(0,6)
plt.xticks(index, labels, fontsize=12, rotation=80)
plt.yticks(fontsize=12)
plt.title('Average percentage of positive words per political party', fontsize=15)

for i in range(len(barlist)):
    barlist[i].set_color(cm(1.0*i/21))

plt.show()
fig14.set_size_inches(14, 8)
fig14.savefig('../images/average_pos_per_party.png', dpi=200, bbox_inches='tight')

#************************************************

#Average negative per political party
fig15 = plt.figure(figsize=(9,6))

average_neg_per_party =   {'ND':128631*100/6301208, 
                              'PASOK':134430*100/6751172, 
                              'Alternative Ecologists':297*100/11929,
                              'SYN':10345*100/429418,
                              'Independent members':6750*100/285232,
                              'Political Spring':1438*100/73841,
                              'KKE':20314*100/958589,
                              'DIKKI':3675*100/171189,
                              'LAOS':6295*100/271228,
                              'SYRIZA':28801*100/1374956,
                              'Golden Dawn':2510*100/111876,
                              'DIMAR':1392*100/59368,
                              'ANEL - Panos Kammenos':2771*100/138048,
                              'KKE Interior':134*100/5882,
                              'DIANA':86*100/3445,
                              'Independent Democratic MPs':314*100/16622,
                              'The River':1518*100/79108,
                              'DISI':4645*100/216030,
                              'Union of Centrists':698*100/43994,
                              'ANEL - NPD Alliance':1060*100/62234,
                              'LAE':373*100/19319
                              }

sorted_average = dict(sorted(average_neg_per_party.items(), key=operator.itemgetter(1)))

labels = [key for key,value in sorted_average.items()]

neg_scores = [value for key,value in sorted_average.items()]

cm = plt.get_cmap('Reds')
#plt.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

index = np.arange(len(labels))
barlist = plt.bar(index, neg_scores, width=0.6)
plt.xlabel('Political parties', fontsize=12)
plt.ylabel('Average percentage of negative words', fontsize=12)
plt.ylim(0,6)
plt.xticks(index, labels, fontsize=12, rotation=80)
plt.yticks(fontsize=12)
plt.title('Average percentage of negative words per political party', fontsize=15)

for i in range(len(barlist)):
    barlist[i].set_color(cm(1.0*i/21))

plt.show()
fig15.set_size_inches(14, 8)
fig15.savefig('../images/average_neg_per_party.png', dpi=200, bbox_inches='tight')
