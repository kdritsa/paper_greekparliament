# -*- coding: utf-8 -*-

import argparse
import pandas as pd
import numpy as np
import math
import os

def get_metrics(df):
    
    metrics={}
    # Simple sum of positive and negatives words detected
    pos_sum = df['pos'].sum()
    neg_sum = df['neg'].sum()
    
    # Square root of the sum of squares divided by the count of non-nan values
    # If denominator is zero, assign float nan value to variable
    smog_Qmean = ((math.sqrt(((df['smog']**2).sum())/(df['smog'].count()))) if df['smog'].count()!=0 else math.nan)
    anger_Qmean = ((math.sqrt(((df['anger']**2).sum())/(df['anger'].count()))) if df['anger'].count()!=0 else math.nan)
    disgust_Qmean = ((math.sqrt(((df['disgust']**2).sum())/(df['disgust'].count()))) if df['disgust'].count()!=0 else math.nan)
    fear_Qmean = ((math.sqrt(((df['fear']**2).sum())/(df['fear'].count()))) if df['fear'].count()!=0 else math.nan)
    happiness_Qmean = ((math.sqrt(((df['happiness']**2).sum())/(df['happiness'].count()))) if df['happiness'].count()!=0 else math.nan)
    sadness_Qmean = ((math.sqrt(((df['sadness']**2).sum())/(df['sadness'].count()))) if df['sadness'].count()!=0 else math.nan)
    surprise_Qmean = ((math.sqrt(((df['surprise']**2).sum())/(df['surprise'].count()))) if df['surprise'].count()!=0 else math.nan)
    
    metrics['pos_sum'] = int(pos_sum)
    metrics['neg_sum'] = int(neg_sum)
    
    metrics['smog_Qmean'] = (((int(smog_Qmean*100))/100) if (not math.isnan(smog_Qmean)) else None)
    metrics['anger_Qmean'] = (((int(anger_Qmean*100))/100) if (not math.isnan(anger_Qmean)) else None)
    metrics['disgust_Qmean'] = (((int(disgust_Qmean*100))/100) if (not math.isnan(disgust_Qmean)) else None)
    metrics['fear_Qmean'] = (((int(fear_Qmean*100))/100) if (not math.isnan(fear_Qmean)) else None)
    metrics['happiness_Qmean'] = (((int(happiness_Qmean*100))/100) if (not math.isnan(happiness_Qmean)) else None)
    metrics['sadness_Qmean'] = (((int(sadness_Qmean*100))/100) if (not math.isnan(sadness_Qmean)) else None)
    metrics['surprise_Qmean'] = (((int(surprise_Qmean*100))/100) if (not math.isnan(surprise_Qmean)) else None)
    
    return(metrics)
    
results = open('results.txt', 'w+', encoding='utf-8')

parser = argparse.ArgumentParser()
parser.add_argument("input_folder_path")
args = parser.parse_args()

analyzed_folder = args.input_folder_path
filenames =  os.listdir(analyzed_folder)

df_all = pd.DataFrame()
list_ = []

# Concat all csv data in one dataframe
for filename in filenames:
    
    df = pd.read_csv(analyzed_folder+filename, header=None)
    list_.append(df)

df_all = pd.concat(list_, ignore_index=True)
  
# Edit dataframe and discard unwanted information

# Discard columns of member name and sitting number
df_all = df_all.drop([0, 2], axis=1)

# Replace date with year only and make it type int
df_all[1] = (df_all[1].apply(lambda x: x.split('-')[0])).astype(int)

# Name columns
df_all.columns = ['year', 'party', 'smog', 'sent6vec', 'pos-neg']

# Remove parentheses from pos-neg and sent6vec columns
df_all['sent6vec'] = df_all['sent6vec'].str[1:-1]
df_all['pos-neg'] = df_all['pos-neg'].str[1:-1]

#split column pos-neg in columns pos and neg
df_all[['pos', 'neg']] = df_all['pos-neg'].str.split(', ',expand=True)

#split 6 sentiment vector in 6 columns
df_all[['anger','disgust', 'fear', 'happiness', 'sadness', 'surprise']] = df_all['sent6vec'].str.split(', ',expand=True)

# Discard original columns, 1 is vertical axis
df_all = df_all.drop(['sent6vec', 'pos-neg'], 1)

# Change column data type to numeric and then convert int zeros to NaN
df_all[['smog','pos', 'neg','anger','disgust', 'fear', 'happiness', 'sadness', 'surprise']] = df_all[['smog','pos', 'neg','anger','disgust', 'fear', 'happiness', 'sadness', 'surprise']].apply(pd.to_numeric)
df_all[['smog','anger','disgust', 'fear', 'happiness', 'sadness', 'surprise']] = df_all[['smog','anger','disgust', 'fear', 'happiness', 'sadness', 'surprise']].replace(0, np.NaN)

#GENERIC METRICS
results.write('---------------------------------------------------')
results.write('\n\n*Generic Parliament Metrics (all times - all parties)*\n')
metrics = get_metrics(df_all)
results.write('\nMetrics: '+str(metrics))




#METRICS PER ERA
results.write('\n\n---------------------------------------------------')
results.write('\n\n*Metrics in each of the 6 eras:*')



results.write('\n\nEra 1989-1993: ')
df_1989_1993 = df_all.loc[df_all['year']<=1993]
metrics = get_metrics(df_1989_1993)
results.write('\nMetrics: '+str(metrics))

results.write('\n\nIn this era.......')
# Get all unique values of column party in this era
parties = (df_1989_1993['party'].unique())
for p in parties:
    results.write('\n\nParty: '+p+': ')
    party_df = df_1989_1993.loc[df_all['party']==p]
    metrics = get_metrics(party_df)
    results.write('\nMetrics: '+str(metrics))

del(party_df)
del df_1989_1993




results.write('\n\nEra 1994-1998: ')
df_1994_1998 = df_all.loc[(df_all['year']>=1994) &( df_all['year']<=1998)]
metrics = get_metrics(df_1994_1998)
results.write('\nMetrics: '+str(metrics))

results.write('\n\nIn this era.......')
# Get all unique values of column party in this era
parties = (df_1994_1998['party'].unique())
for p in parties:
    results.write('\n\nParty: '+p+': ')
    party_df = df_1994_1998.loc[df_all['party']==p]
    metrics = get_metrics(party_df)
    results.write('\nMetrics: '+str(metrics))

del(party_df)
del df_1994_1998




results.write('\n\nEra 1999-2003: ')
df_1999_2003 = df_all.loc[(df_all['year']>=1999) &( df_all['year']<=2003)]
metrics = get_metrics(df_1999_2003)
results.write('\nMetrics: '+str(metrics))

results.write('\n\nIn this era.......')
# Get all unique values of column party in this era
parties = (df_1999_2003['party'].unique())
for p in parties:
    results.write('\n\nParty: '+p+': ')
    party_df = df_1999_2003.loc[df_all['party']==p]
    metrics = get_metrics(party_df)
    results.write('\nMetrics: '+str(metrics))

del(party_df)
del df_1999_2003




results.write('\n\nEra 2004-2008: ')
df_2004_2008 = df_all.loc[(df_all['year']>=2004) &( df_all['year']<=2008)]
metrics = get_metrics(df_2004_2008)
results.write('\nMetrics: '+str(metrics))

results.write('\n\nIn this era.......')
# Get all unique values of column party in this era
parties = (df_2004_2008['party'].unique())
for p in parties:
    results.write('\n\nParty: '+p+': ')
    party_df = df_2004_2008.loc[df_all['party']==p]
    metrics = get_metrics(party_df)
    results.write('\nMetrics: '+str(metrics))

del(party_df)
del df_2004_2008




results.write('\n\nEra 2009-2013: ')
df_2009_2013 = df_all.loc[(df_all['year']>=2009) &( df_all['year']<=2013)]
metrics = get_metrics(df_2009_2013)
results.write('\nMetrics: '+str(metrics))

results.write('\n\nIn this era.......')
# Get all unique values of column party in this era
parties = (df_2009_2013['party'].unique())
for p in parties:
    results.write('\n\nParty: '+p+': ')
    party_df = df_2009_2013.loc[df_all['party']==p]
    metrics = get_metrics(party_df)
    results.write('\nMetrics: '+str(metrics))

del(party_df)
del df_2009_2013




results.write('\n\nEra 2014-2017: ')
df_2014_2017 = df_all.loc[(df_all['year']>=2014)]
metrics = get_metrics(df_2014_2017)
results.write('\nMetrics: '+str(metrics))

results.write('\n\nIn this era.......')
# Get all unique values of column party in this era
parties = (df_2014_2017['party'].unique())
for p in parties:
    results.write('\n\nParty: '+p+': ')
    party_df = df_2014_2017.loc[df_all['party']==p]
    metrics = get_metrics(party_df)
    results.write('\nMetrics: '+str(metrics))

del(party_df)
del df_2014_2017



# METRICS PER PARTY
results.write('\n\n---------------------------------------------------')
results.write('\n\n*Metrics of each political party throughout all times*')

# Get all unique values of column party
parties = (df_all['party'].unique())

for p in parties:
    results.write('\n\nParty: '+p+': ')
    party_df = df_all.loc[df_all['party']==p]
    metrics = get_metrics(party_df)
    results.write('\nMetrics: '+str(metrics))
    del(party_df)




results.close()


