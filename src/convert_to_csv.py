import csv
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument("input")
parser.add_argument("output")
args = parser.parse_args()

speech_p = re.compile(r',speech:\s*')

with open(args.input, encoding = 'utf-8') as input_file, open(args.output, 'w', encoding = 'utf-8') as output_file:
    csv_output = csv.writer(output_file)
    speech = ''
    row = []
    for line in input_file:
        speech_m = speech_p.search(line)
        if speech_m:
            if len(row) > 0:
                row.append(speech.rstrip())
                csv_output.writerow(row)            
            row = line[:speech_m.start()].split(',')
            speech = line[speech_m.end():]
        else:
            speech += line
    row.append(speech.rstrip())            
    csv_output.writerow(row)
                
    
