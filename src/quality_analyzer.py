from collections import defaultdict
from smog_calculator_gr import smog_index

f = open('tell_all.txt', 'r+', encoding='utf-8') #encoding for Greek
tellall = f.readlines()

all_all=''

all_eras_party = defaultdict(str)
all_parties_era = defaultdict(str)

era1_parties = defaultdict(str)
era2_parties = defaultdict(str)
era3_parties = defaultdict(str)
era4_parties = defaultdict(str)
era5_parties = defaultdict(str)
era6_parties = defaultdict(str)

counter=0

for line in tellall:
    year = (line.split(',')[1]).split('-')[0]
    party = line.split(',')[2]
    speech = (line.split('speech:')[1]).rstrip('\n') #remove trailing newlines
    counter+=1
    if counter%5000==0:
        print(counter)
    all_all+=speech    
    all_eras_party[party]+=speech
#    
    if int(year)<1994:
        era1_parties[party]+=speech
        all_parties_era['era1']+=speech
    elif int(year)<1999:
        era2_parties[party]+=speech
        all_parties_era['era2']+=speech
    elif int(year)<2004:
        era3_parties[party]+=speech
        all_parties_era['era3']+=speech
    elif int(year)<2009:
        era4_parties[party]+=speech
        all_parties_era['era4']+=speech
    elif int(year)<2014:
        era5_parties[party]+=speech
        all_parties_era['era5']+=speech
    else:
        era6_parties[party]+=speech
        all_parties_era['era6']+=speech

print('\n\n*Generic Parliament Metrics (all times - all parties)*\n')
print(smog_index(all_all))
print('end')
#
print('\n\n*Smog index in each of the 6 eras:*\n')
for era, speech in all_parties_era.items():
    print('\n'+era+': '+smog_index(speech))


#Metrics concerning parties at all times
print('\nSmog index of each political party throughout all times')
for party, speech in all_eras_party.items():
    print('\n'+party+': '+str(smog_index(speech)))

#Metrics concerning each party in each era
print('\nSmog index of each political party at each era')
print('\nEra 1989-1993: ')
for party, speech in era1_parties.itmes():
    print('\n'+party+': '+smog_index(speech))

print('\nEra 1994-1998: ')
for party, speech in era2_parties.itmes():
    print('\n'+party+': '+smog_index(speech))

print('\nEra 1999-2003: ')
for party, speech in era3_parties.itmes():
    print('\n'+party+': '+smog_index(speech))

print('\nEera 2004-2008: ')
for party, speech in era4_parties.itmes():
    print('\n'+party+': '+smog_index(speech))

print('\nEra 2009-2013: ')
for party, speech in era5_parties.itmes():
    print('\n'+party+': '+smog_index(speech))

print('\nEra 2014-2017: ')
for party, speech in era6_parties.itmes():
    print('\n'+party+': '+smog_index(speech))

f.close()