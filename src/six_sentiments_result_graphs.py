# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import operator

fig1 = plt.figure(figsize=(6,5))
plt.title('Average sentiments through time', fontsize=15)

time = [1991, 1996, 2001, 2006, 2011, 2016]
anger_per_era =     [3.46, 3.43, 3.41, 3.33, 3.33, 3.29]
disgust_per_era =   [3.21, 3.20, 3.18, 3.11, 3.10, 3.04]
fear_per_era =      [2.22, 2.22, 2.20, 2.17, 2.15, 2.10]
happiness_per_era = [1.97, 2.03, 2.05, 2.08, 2.06, 2.03]
sadness_per_era =   [1.21, 1.21, 1.22, 1.28, 1.21, 1.24]
surprise_per_era =  [3.63, 3.61, 3.59, 3.56, 3.55, 3.53]

line1, = plt.plot(time,anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time, sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time, surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0, 5) 
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig1.set_size_inches(14, 8)
fig1.savefig('../images/average_sentiments_through_time.png', dpi=200, bbox_inches='tight')

#************************************************
fig2 = plt.figure(figsize=(9,6))
plt.title('ND - Average sentiments through time', fontsize=15)

#6 sentiments of party ND
time = [1991, 1996, 2001, 2006, 2011, 2016]
nd_anger_per_era =     [3.52, 3.42, 3.38, 3.36, 3.34, 3.31]
nd_disgust_per_era =   [3.26, 3.19, 3.16, 3.13, 3.09, 3.05]
nd_fear_per_era =      [2.25, 2.22, 2.19, 2.18, 2.14, 2.11]
nd_happiness_per_era = [1.98, 2.00, 2.03, 2.11, 2.06, 2.04]
nd_sadness_per_era =   [1.21, 1.21, 1.22, 1.28, 1.21, 1.23]
nd_surprise_per_era =  [3.68, 3.59, 3.57, 3.59, 3.56, 3.53]


line1, = plt.plot(time,nd_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,nd_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,nd_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,nd_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,nd_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,nd_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig2.set_size_inches(14, 8)
fig2.savefig('../images/nd_sentiments_through_time.png', dpi=200, bbox_inches='tight')

#************************************************
fig3 = plt.figure(figsize=(9,6))
plt.title('KKE - Average sentiments through time', fontsize=15)

#6 sentiments of party KKE
time = [1991, 1996, 2001, 2006, 2011, 2016]
kke_anger_per_era =     [3.39, 3.49, 3.43, 3.34, 3.26, 3.32]
kke_disgust_per_era =   [3.13, 3.26, 3.20, 3.11, 3.03, 3.03]
kke_fear_per_era =      [2.16, 2.24, 2.20, 2.15, 2.07, 2.09]
kke_happiness_per_era = [1.92, 1.97, 1.94, 1.95, 1.88, 1.94]
kke_sadness_per_era =   [1.20, 1.20, 1.21, 1.28, 1.20, 1.29]
kke_surprise_per_era =  [3.53, 3.65, 3.60, 3.53, 3.44, 3.55]

line1, = plt.plot(time,kke_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,kke_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,kke_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,kke_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,kke_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,kke_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig3.set_size_inches(14, 8)
fig3.savefig('../images/kke_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig4 = plt.figure(figsize=(9,6))
plt.title('PASOK - Average sentiments through time', fontsize=15)

#6 sentiments of party PASOK
time = [1991, 1996, 2001, 2006, 2011, 2016]
pasok_anger_per_era =     [3.42, 3.45, 3.43, 3.30, 3.36, 3.34]
pasok_disgust_per_era =   [3.17, 3.22, 3.20, 3.08, 3.13, 3.09]
pasok_fear_per_era =      [2.19, 2.23, 2.21, 2.17, 2.17, 2.13]
pasok_happiness_per_era = [1.98, 2.06, 2.08, 2.06, 2.08, 1.99]
pasok_sadness_per_era =   [1.21, 1.22, 1.22, 1.27, 1.21, 1.20]
pasok_surprise_per_era =  [3.60, 3.62, 3.62, 3.53, 3.58, 3.55]

line1, = plt.plot(time,pasok_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,pasok_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,pasok_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,pasok_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,pasok_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,pasok_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig4.set_size_inches(14, 8)
fig4.savefig('../images/pasok_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig5 = plt.figure(figsize=(9,6))
plt.title('SYRIZA - Average sentiments through time', fontsize=15)

#6 sentiments of party SYRIZA
time = [1991, 1996, 2001, 2006, 2011, 2016]
syriza_anger_per_era =     [None, None, None, 3.30, 3.35, 3.31]
syriza_disgust_per_era =   [None, None, None, 3.08, 3.11, 3.07]
syriza_fear_per_era =      [None, None, None, 2.18, 2.15, 2.12]
syriza_happiness_per_era = [None, None, None, 2.10, 2.05, 2.07]
syriza_sadness_per_era =   [None, None, None, 1.30, 1.20, 1.22]
syriza_surprise_per_era =  [None, None, None, 3.52, 3.58, 3.55]

line1, = plt.plot(time,syriza_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,syriza_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,syriza_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,syriza_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,syriza_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,syriza_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig5.set_size_inches(14, 8)
fig5.savefig('../images/syriza_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig6 = plt.figure(figsize=(9,6))
plt.title('DIMAR - Average sentiments through time', fontsize=15)

#6 sentiments of party DIMAR
time = [1991, 1996, 2001, 2006, 2011, 2016]
dimar_anger_per_era =     [None, None, None, None, 3.27, 3.21]
dimar_disgust_per_era =   [None, None, None, None, 3.04, 2.99]
dimar_fear_per_era =      [None, None, None, None, 2.13, 2.07]
dimar_happiness_per_era = [None, None, None, None, 2.12, 1.93]
dimar_sadness_per_era =   [None, None, None, None, 1.17, 1.21]
dimar_surprise_per_era =  [None, None, None, None, 3.52, 3.49]

line1, = plt.plot(time,dimar_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,dimar_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,dimar_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,dimar_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,dimar_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,dimar_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig6.set_size_inches(14, 8)
fig6.savefig('../images/dimar_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig7 = plt.figure(figsize=(9,6))
plt.title('ANEL (Panos Kammenos) - Average sentiments through time', fontsize=15)

#6 sentiments of party ANEL - Panos Kammenos
time = [1991, 1996, 2001, 2006, 2011, 2016]
anel1_anger_per_era =     [None, None, None, None, 3.32, 3.34]
anel1_disgust_per_era =   [None, None, None, None, 3.10, 3.10]
anel1_fear_per_era =      [None, None, None, None, 2.15, 2.15]
anel1_happiness_per_era = [None, None, None, None, 2.02, 1.95]
anel1_sadness_per_era =   [None, None, None, None, 1.23, 1.20]
anel1_surprise_per_era =  [None, None, None, None, 3.54, 3.54]

line1, = plt.plot(time,anel1_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,anel1_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,anel1_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,anel1_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,anel1_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,anel1_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig7.set_size_inches(14, 8)
fig7.savefig('../images/anel1_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig8 = plt.figure(figsize=(9,6))
plt.title('LAOS - Average sentiments through time', fontsize=15)

#6 sentiments of party LAOS
time = [1991, 1996, 2001, 2006, 2011, 2016]
laos_anger_per_era =     [None, None, None, 3.30, 3.29, None]
laos_disgust_per_era =   [None, None, None, 3.09, 3.05, None]
laos_fear_per_era =      [None, None, None, 2.17, 2.11, None]
laos_happiness_per_era = [None, None, None, 2.11, 2.13, None]
laos_sadness_per_era =   [None, None, None, 1.24, 1.20, None]
laos_surprise_per_era =  [None, None, None, 3.55, 3.53, None]

line1, = plt.plot(time,laos_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,laos_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,laos_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,laos_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,laos_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,laos_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig8.set_size_inches(14, 8)
fig8.savefig('../images/laos_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig9 = plt.figure(figsize=(9,6))
plt.title('Golden Dawn - Average sentiments through time', fontsize=15)

#6 sentiments of party Golden Dawn
time = [1991, 1996, 2001, 2006, 2011, 2016]
gd_anger_per_era =     [None, None, None, None, 3.38, 3.25]
gd_disgust_per_era =   [None, None, None, None, 3.16, 2.95]
gd_fear_per_era =      [None, None, None, None, 2.16, 2.02]
gd_happiness_per_era = [None, None, None, None, 1.96, 1.92]
gd_sadness_per_era =   [None, None, None, None, 1.20, 1.29]
gd_surprise_per_era =  [None, None, None, None, 3.58, 3.53]

line1, = plt.plot(time,gd_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,gd_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,gd_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,gd_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,gd_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,gd_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig9.set_size_inches(14, 8)
fig9.savefig('../images/gd_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig10 = plt.figure(figsize=(9,6))
plt.title('DIKKI - Average sentiments through time', fontsize=15)

#6 sentiments of party DIKKI
time = [1991, 1996, 2001, 2006, 2011, 2016]
dikki_anger_per_era =     [None, 3.41, 3.40, None, None, None]
dikki_disgust_per_era =   [None, 3.18, 3.17, None, None, None]
dikki_fear_per_era =      [None, 2.24, 2.21, None, None, None]
dikki_happiness_per_era = [None, 2.02, 2.02, None, None, None]
dikki_sadness_per_era =   [None, 1.22, 1.23, None, None, None]
dikki_surprise_per_era =  [None, 3.56, 3.60, None, None, None]

line1, = plt.plot(time,dikki_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,dikki_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,dikki_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,dikki_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,dikki_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,dikki_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig10.set_size_inches(14, 8)
fig10.savefig('../images/dikki_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig11 = plt.figure(figsize=(9,6))
plt.title('Political Spring - Average sentiments through time', fontsize=15)

#6 sentiments of party Political Spring
time = [1991, 1996, 2001, 2006, 2011, 2016]
ps_anger_per_era =     [3.36, 3.39, None, None, None, None]
ps_disgust_per_era =   [3.07, 3.15, None, None, None, None]
ps_fear_per_era =      [2.13, 2.16, None, None, None, None]
ps_happiness_per_era = [2.15, 1.98, None, None, None, None]
ps_sadness_per_era =   [1.24, 1.22, None, None, None, None]
ps_surprise_per_era =  [3.56, 3.59, None, None, None, None]

line1, = plt.plot(time,ps_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,ps_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,ps_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,ps_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,ps_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,ps_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig11.set_size_inches(14, 8)
fig11.savefig('../images/ps_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig12 = plt.figure(figsize=(9,6))
plt.title('Independent Members - Average sentiments through time', fontsize=15)

#6 sentiments of Independent Members
time = [1991, 1996, 2001, 2006, 2011, 2016]
im_anger_per_era =     [3.38, None, 3.06, 3.19, 3.31, 3.30]
im_disgust_per_era =   [3.16, None, 2.91, 2.96, 3.08, 3.03]
im_fear_per_era =      [2.18, None, 2.05, 2.13, 2.17, 2.09]
im_happiness_per_era = [2.01, None, 1.97, 2.22, 2.06, 2.05]
im_sadness_per_era =   [1.24, None, 1.41, 1.17, 1.20, 1.27]
im_surprise_per_era =  [3.59, None, 3.30, 3.47, 3.55, 3.55]

line1, = plt.plot(time,im_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,im_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,im_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,im_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,im_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,im_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig12.set_size_inches(14, 8)
fig12.savefig('../images/im_sentiments_through_time.png', dpi=200, bbox_inches='tight')
#************************************************
fig13 = plt.figure(figsize=(9,6))
plt.title('SYN - Average sentiments through time', fontsize=15)

#6 sentiments of party SYN
time = [1991, 1996, 2001, 2006, 2011, 2016]
syn_anger_per_era =     [3.42, 3.29, 3.30, 3.30, None, None]
syn_disgust_per_era =   [3.18, 3.08, 3.11, 3.09, None, None]
syn_fear_per_era =      [2.18, 2.21, 2.17, 2.15, None, None]
syn_happiness_per_era = [1.94, 2.12, 1.99, 1.93, None, None]
syn_sadness_per_era =   [1.19, 1.24, 1.19, 1.06, None, None]
syn_surprise_per_era =  [3.59, 3.51, 3.49, 3.51, None, None]

line1, = plt.plot(time,syn_anger_per_era, '#FF0000', label='Anger')
line2, = plt.plot(time,syn_disgust_per_era, '#4EC247', label='Disgust')
line3, = plt.plot(time,syn_fear_per_era, 'm-', label='Fear')
line4, = plt.plot(time,syn_happiness_per_era, '#20B2AA', label='Happiness')
line5, = plt.plot(time,syn_sadness_per_era,'#000000', label='Sandess')
line6, = plt.plot(time,syn_surprise_per_era, '#FFA500', label='Surprise')

plt.legend()
plt.xlabel('time', fontsize=12)
plt.ylabel('sentiments', fontsize=12)
plt.ylim(0,5)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.show()
fig13.set_size_inches(14, 8)
fig13.savefig('../images/syn_sentiments_through_time.png', dpi=200, bbox_inches='tight')

#************************************************

#Average anger per political party
fig14 = plt.figure(figsize=(9,6))

average_anger_per_party = {'ND':3.39, 
                          'PASOK':3.39, 
                          'Alternative Ecologists':3.42,
                          'SYN':3.36,
                          'Independent members':3.32,
                          'Political Spring':3.39,
                          'KKE':3.35,
                          'DIKKI':3.41,
                          'LAOS':3.29,
                          'SYRIZA':3.32,
                          'Golden Dawn':3.30,
                          'DIMAR':3.25,
                          'ANEL - Panos Kammenos':3.33,
                          'KKE Interior':3.48,
                          'DIANA':3.39,
                          'Independent Democratic MPs':3.26,
                          'The River':3.22,
                          'DISI':3.25,
                          'Union of Centrists':3.23,
                          'ANEL - NPD Alliance':3.25,
                          'LAE':3.32
                          }

sorted_average = dict(sorted(average_anger_per_party.items(), key=operator.itemgetter(1)))

labels = [key for key,value in sorted_average.items()]

anger_scores = [value for key,value in sorted_average.items()]

cm = plt.get_cmap('Reds')
#plt.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

index = np.arange(len(labels))
barlist = plt.bar(index, anger_scores, width=0.6)
plt.xlabel('Political parties', fontsize=12)
plt.ylabel('Average anger score', fontsize=12)
plt.ylim(0,5)
plt.xticks(index, labels, fontsize=12, rotation=80)
plt.yticks(fontsize=12)
plt.title('Average anger score per political party', fontsize=15)

for i in range(len(barlist)):
    barlist[i].set_color(cm(1.0*i/21))

plt.show()
fig14.set_size_inches(14, 8)
fig14.savefig('../images/average_anger_per_party.png', dpi=200, bbox_inches='tight')

#************************************************

#Average disgust per political party
fig15 = plt.figure(figsize=(9,6))

average_disgust_per_party =   {'ND':3.15, 
                              'PASOK':3.16, 
                              'Alternative Ecologists':3.19,
                              'SYN':3.14,
                              'Independent members':3.08,
                              'Political Spring':3.14,
                              'KKE':3.11,
                              'DIKKI':3.18,
                              'LAOS':3.06,
                              'SYRIZA':3.08,
                              'Golden Dawn':3.03,
                              'DIMAR':3.03,
                              'ANEL - Panos Kammenos':3.10,
                              'KKE Interior':3.24,
                              'DIANA':3.15,
                              'Independent Democratic MPs':3.02,
                              'The River':2.93,
                              'DISI':2.99,
                              'Union of Centrists':2.89,
                              'ANEL - NPD Alliance':2.99,
                              'LAE':3.08
                              }

sorted_average = dict(sorted(average_disgust_per_party.items(), key=operator.itemgetter(1)))

labels = [key for key,value in sorted_average.items()]

disgust_scores = [value for key,value in sorted_average.items()]

cm = plt.get_cmap('Greens')
#plt.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

index = np.arange(len(labels))
barlist = plt.bar(index, disgust_scores, width=0.6)
plt.xlabel('Political parties', fontsize=12)
plt.ylabel('Average disgust score', fontsize=12)
plt.ylim(0,5)
plt.xticks(index, labels, fontsize=12, rotation=80)
plt.yticks(fontsize=12)
plt.title('Average disgust score per political party', fontsize=15)

for i in range(len(barlist)):
    barlist[i].set_color(cm(1.0*i/21))

plt.show()
fig15.set_size_inches(14, 8)
fig15.savefig('../images/average_disgust_per_party.png', dpi=200, bbox_inches='tight')
#************************************************

#Average fear per political party
fig16 = plt.figure(figsize=(9,6))

average_fear_per_party = {'ND':2.19, 
                          'PASOK':2.19, 
                          'Alternative Ecologists':2.23,
                          'SYN':2.19,
                          'Independent members':2.15,
                          'Political Spring':2.15,
                          'KKE':2.14,
                          'DIKKI':2.23,
                          'LAOS':2.12,
                          'SYRIZA':2.14,
                          'Golden Dawn':2.07,
                          'DIMAR':2.11,
                          'ANEL - Panos Kammenos':2.15,
                          'KKE Interior':2.20,
                          'DIANA':2.18,
                          'Independent Democratic MPs':2.08,
                          'The River':2.04,
                          'DISI':2.07,
                          'Union of Centrists':2.02,
                          'ANEL - NPD Alliance':2.07,
                          'LAE':2.10
                          }

sorted_average = dict(sorted(average_fear_per_party.items(), key=operator.itemgetter(1)))

labels = [key for key,value in sorted_average.items()]

fear_scores = [value for key,value in sorted_average.items()]

cm = plt.get_cmap('Purples')
#plt.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

index = np.arange(len(labels))
barlist = plt.bar(index, fear_scores, width=0.6)
plt.xlabel('Political parties', fontsize=12)
plt.ylabel('Average fear score', fontsize=12)
plt.ylim(0,5)
plt.xticks(index, labels, fontsize=12, rotation=80)
plt.yticks(fontsize=12)

plt.title('Average fear score per political party', fontsize=15)

for i in range(len(barlist)):
    barlist[i].set_color(cm(1.0*i/21))

plt.show()
fig16.set_size_inches(14, 8)
fig16.savefig('../images/average_fear_per_party.png', dpi=200, bbox_inches='tight')
#************************************************

#Average happiness per political party
fig17 = plt.figure(figsize=(9,6))

average_happiness_per_party = {'ND':2.05, 
                              'PASOK':2.05, 
                              'Alternative Ecologists':1.86,
                              'SYN':1.99,
                              'Independent members':2.06,
                              'Political Spring':2.00,
                              'KKE':1.93,
                              'DIKKI':2.02,
                              'LAOS':2.13,
                              'SYRIZA':2.07,
                              'Golden Dawn':1.93,
                              'DIMAR':2.06,
                              'ANEL - Panos Kammenos':2.00,
                              'KKE Interior':2.04,
                              'DIANA':2.01,
                              'Independent Democratic MPs':2.05,
                              'The River':2.02,
                              'DISI':2.01,
                              'Union of Centrists':2.08,
                              'ANEL - NPD Alliance':2.08,
                              'LAE':2.10
                              }

sorted_average = dict(sorted(average_happiness_per_party.items(), key=operator.itemgetter(1)))

labels = [key for key,value in sorted_average.items()]

happiness_scores = [value for key,value in sorted_average.items()]

cm = plt.get_cmap('Blues')
#plt.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

index = np.arange(len(labels))
barlist = plt.bar(index, happiness_scores, width=0.6)
plt.xlabel('Political parties', fontsize=12)
plt.ylabel('Average happiness score', fontsize=12)
plt.ylim(0,5)
plt.xticks(index, labels, fontsize=12, rotation=80)
plt.yticks(fontsize=12)
plt.title('Average happiness score per political party', fontsize=15)

for i in range(len(barlist)):
    barlist[i].set_color(cm(1.0*i/21))

plt.show()
fig17.set_size_inches(14, 8)
fig17.savefig('../images/average_happiness_per_party.png', dpi=200, bbox_inches='tight')
#************************************************

#Average sadness per political party
fig18 = plt.figure(figsize=(9,6))

average_sadness_per_party =  {'ND':1.23, 
                              'PASOK':1.22, 
                              'Alternative Ecologists':1.25,
                              'SYN':1.20,
                              'Independent members':1.23,
                              'Political Spring':1.22,
                              'KKE':1.24,
                              'DIKKI':1.22,
                              'LAOS':1.21,
                              'SYRIZA':1.22,
                              'Golden Dawn':1.26,
                              'DIMAR':1.18,
                              'ANEL - Panos Kammenos':1.22,
                              'KKE Interior':1.09,
                              'DIANA':1.25,
                              'Independent Democratic MPs':1.18,
                              'The River':1.31,
                              'DISI':1.23,
                              'Union of Centrists':1.38,
                              'ANEL - NPD Alliance':1.24,
                              'LAE':1.30
                              }

sorted_average = dict(sorted(average_sadness_per_party.items(), key=operator.itemgetter(1)))

labels = [key for key,value in sorted_average.items()]

sadness_scores = [value for key,value in sorted_average.items()]

cm = plt.get_cmap('bone')
#plt.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

index = np.arange(len(labels))
barlist = plt.bar(index, sadness_scores, width=0.6)
plt.xlabel('Political parties', fontsize=12)
plt.ylabel('Average sadness score', fontsize=12)
plt.ylim(0,5)
plt.xticks(index, labels, fontsize=12, rotation=80)
plt.yticks(fontsize=12)
plt.title('Average sadness score per political party', fontsize=15)

for i in range(len(barlist)):
    barlist[i].set_color(cm(1.0*(21-i)/21)) #reversed colormap

plt.show()
fig18.set_size_inches(14, 8)
fig18.savefig('../images/average_sadness_per_party.png', dpi=200, bbox_inches='tight')

#************************************************

#Average surprise per political party
fig19 = plt.figure(figsize=(9,6))

average_surprise_per_party = {'ND':3.59, 
                              'PASOK':3.59, 
                              'Alternative Ecologists':3.56,
                              'SYN':3.54,
                              'Independent members':3.55,
                              'Political Spring':3.59,
                              'KKE':3.54,
                              'DIKKI':3.58,
                              'LAOS':3.53,
                              'SYRIZA':3.56,
                              'Golden Dawn':3.55,
                              'DIMAR':3.51,
                              'ANEL - Panos Kammenos':3.54,
                              'KKE Interior':3.61,
                              'DIANA':3.53,
                              'Independent Democratic MPs':3.47,
                              'The River':3.47,
                              'DISI':3.47,
                              'Union of Centrists':3.55,
                              'ANEL - NPD Alliance':3.51,
                              'LAE':3.56
                              }
sorted_average = dict(sorted(average_surprise_per_party.items(), key=operator.itemgetter(1)))

labels = [key for key,value in sorted_average.items()]

surprise_scores = [value for key,value in sorted_average.items()]

cm = plt.get_cmap('YlOrBr')
#plt.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

index = np.arange(len(labels))
barlist = plt.bar(index, surprise_scores, width=0.6)
plt.xlabel('Political parties', fontsize=12)
plt.ylabel('Average surprise score', fontsize=12)
plt.ylim(0,5)
plt.xticks(index, labels, fontsize=12, rotation=80)
plt.yticks(fontsize=12)
plt.title('Average surprise score per political party', fontsize=15)

for i in range(len(barlist)):
    barlist[i].set_color(cm(1.0*i/21))

plt.show()
fig19.set_size_inches(14, 8)
fig19.savefig('../images/average_surprise_per_party.png', dpi=200, bbox_inches='tight')