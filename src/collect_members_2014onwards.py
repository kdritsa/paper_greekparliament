# -*- coding: utf-8 -*-
import re
import datetime
from datetime import timedelta, date
from collections import defaultdict
import pandas as pd
import numpy as np

def check_date_range(start_date, end_date, limit='2014-01-01'):

    return start_date, end_date


def party_formatting(party):
    if party=='ΑΝΕΞΑΡΤΗΤΟΙΕΛΛΗΝΕΣΕΘΝΙΚΗΠΑΤΡΙΩΤΙΚΗΔΗΜΟΚΡΑΤΙΚΗΣΥΜΜΑΧΙΑ':
        party='ανεξαρτητοι ελληνες εθνικη πατριωτικη δημοκρατικη συμμαχια'
    elif party=='ΑΝΕΞΑΡΤΗΤΟΙΕΛΛΗΝΕΣ-ΠΑΝΟΣΚΑΜΜΕΝΟΣ':
        party='ανεξαρτητοι ελληνες - πανος καμμενος'
    elif party=='ΑΝΕΞΑΡΤΗΤΟΙ':
        party= 'ανεξαρτητοι (εκτος κομματος)'
    elif party=='ΣΥΝΑΣΠΙΣΜΟΣ':
        party='συνασπισμος της αριστερας των κινηματων και της οικολογιας'
    elif party=='ΑΝΕΞΑΡΤΗΤΟΙΔΗΜΟΚΡΑΤΙΚΟΙΒΟΥΛΕΥΤΕΣ':
        party='ανεξαρτητοι δημοκρατικοι βουλευτες'
    elif party=='ΠΟΛ.Α.':
        party='πολιτικη ανοιξη'
    elif party=='ΟΟ.ΕΟ.':
        party='οικολογοι εναλλακτικοι (ομοσπονδια οικολογικων εναλλακτικων οργανωσεων)'
    elif party=='ΔΗ.ΑΝΑ.':
        party= 'δημοκρατικη ανανεωση'
    elif party=='ΔΗ.Κ.ΚΙ.':
        party='δημοκρατικο κοινωνικο κινημα'
    elif party=='ΕΝΩΣΗΚΕΝΤΡΩΩΝ':
        party='ενωση κεντρωων'
    elif party== 'ΝΕΑΔΗΜΟΚΡΑΤΙΑ':
        party='νεα δημοκρατια'
    elif party=='ΛΑ.Ο.Σ.':
        party= 'λαικος ορθοδοξος συναγερμος'
    elif party=='ΛΑΪΚΟΣΣΥΝΔΕΣΜΟΣ-ΧΡΥΣΗΑΥΓΗ':
        party='λαικος συνδεσος - χρυση αυγη'
    elif party=='ΚΟΜΜΟΥΝΙΣΤΙΚΟΚΟΜΜΑΕΛΛΑΔΑΣ':
        party='κομμουνιστικο κομμα ελλαδας'
    elif party=='Κ.Κ.Εσ':
        party='κομμουνιστικο κομμα ελλαδας εσωτερικου'
    elif party=='ΣΥΝΑΣΠΙΣΜΟΣΡΙΖΟΣΠΑΣΤΙΚΗΣΑΡΙΣΤΕΡΑΣ':
        party='συνασπισμος ριζοσπαστικης αριστερας'
    elif party=='ΛΑΪΚΗΕΝΟΤΗΤΑ':
        party='λαικη ενοτητα'
    elif party=='ΠΑ.ΣΟ.Κ.':
        party='πανελληνιο σοσιαλιστικο κινημα'
    elif party== 'ΔΗΜΟΚΡΑΤΙΚΗΑΡΙΣΤΕΡΑ':
        party='δημοκρατικη αριστερα'
    elif party=='ΔΗΜΟΚΡΑΤΙΚΗΣΥΜΠΑΡΑΤΑΞΗ(ΠΑΝΕΛΛΗΝΙΟΣΟΣΙΑΛΙΣΤΙΚΟΚΙΝΗΜΑ-ΔΗΜΟΚΡΑΤΙΚΗΑΡΙΣΤΕΡΑ)':
        party='δημοκρατικη συμπαραταξη (πανελληνιο σοσιαλιστικο κινημα - δημοκρατικη αριστερα)'
    elif party=='ΤΟΠΟΤΑΜΙ':
        party='το ποταμι'
    elif party=='ΕΝΩΣΗΚΕΝΤΡΟΥ-ΝΕΕΣΔΥΝΑΜΕΙΣΕΚ/ΝΔ':
        party='ενωση κεντρου - νεες δυναμεις (ε.κ. - ν.δ.)'
    elif party=='ΕΔΗΚ':
        party='ενωση δημοκρατικου κεντρου (εδηκ)'
    elif party=='ΕΘΝΙΚΗΠΑΡΑΤΑΞΙΣ':
        party='εθνική παράταξη'
    elif party=='ΕΘΝΙΚΗΠΑΡΑΤΑΞΙΣ':
        party='εθνικη παραταξη'
    elif party=='ΝΕΟΦΙΛΕΛΕΥΘΕΡΩΝ':
        party='κομμα νεοφιλελευθερων'
    elif party=='ΕΝΙΑΙΑΔΗΜΟΚΡΑΤΙΚΗΑΡΙΣΤΕΡΑ-Ε.Δ.Α.':
        party='ενιαια δημοκρατικη αριστερα (ε.δ.α.)'
    elif party=='ΣΥΜ/ΧΙΑΠΡ':
        party='συμμαχια προοδευτικων και αριστερων δυναμεων'
    else:
        pass;
    return party

def name_formatting(name):

    name = re.sub(r"(\s*-\s*)|(\sή\s)",'-', name) #when people have two surnames or two names, we glue them together with '-' in the middle
    name = name.translate(str.maketrans('άέόώήίϊΐύϋΰ','αεοωηιιιυυυ')) #remove accents
    name = re.sub(r"\t+" , " ", name) #replace tabs with space
    name = re.sub(r"\s\s+" , " ", name) #replace more than one spaces with one space
    name = re.sub(r"(συζ.\s)",'συζ.', name) #remove space between συζ. and the name of the husband
    name = re.sub("μαρια γλυκερια",'μαρια-γλυκερια', name)
    name = re.sub("χατζη χαβουζ γκαληπ",'χατζη-χαβουζ-γκαληπ', name)
    name = re.sub("μακρη θεοδωρου",'μακρη-θεοδωρου', name)
    name = re.sub("καρα γιουσουφ",'καρα-γιουσουφ', name)
    name = re.sub('χατζη οσμαν','χατζη-οσμαν', name)
    name = re.sub("σαδικ αμετ αμετ σαδηκ",'σαδικ αμετ αμετ', name)
    name = re.sub('ακριτα χα λουκη συλβα-καιτη','ακριτα συζ.λουκη συλβα-καιτη', name)
    name = name.rstrip() #remove trailing space from string
    return name

f = open('../out_files/original_members_data.csv', 'r+', encoding='utf-8')

df = pd.read_csv(f, encoding='utf-8', sep=',', header=None)

#rename columns
df.columns = ['no', 'member_name', 'period_date_range', 'event_date',
         'administrative_region', 'political_party',  'event_description']

# remove lines that contain "NO DATA"
df = df[~df['period_date_range'].str.contains("NO DATA")]

df.drop('administrative_region', axis=1, inplace=True)

# remove characters from period
df['period_date_range'] = df['period_date_range'].\
    str.replace(r"[a-zA-Zα-ωΑ-Ω΄':()]", '')

# split dates of period start & end and create new columns
dates = df['period_date_range'].str.split('-', n=1, expand=True)

df['period_start_date'] = dates[0]
df['period_start_date'] = pd.to_datetime(df['period_start_date'],
                                         format='%d/%m/%Y')
df['period_end_date'] = dates[1]
df['period_end_date'] = pd.to_datetime(df['period_end_date'],
                                         format='%d/%m/%Y')

# drop old column
df.drop(columns=['period_date_range'], inplace=True)

# keep years from 2014 onwards
df = df[(df['period_end_date'].dt.year >= 2014)]

# replace not needed strings in data cells
df['member_name'] = df['member_name'].str.replace("Name:", '')
df['event_date'] = df['event_date'].str.replace("Date:", '')
df['event_date'] = pd.to_datetime(df['event_date'], format='%d/%m/%Y')
df['political_party'] = df['political_party'].str.replace(
    "Parliamentary-Party:", '')
df['event_description'] = df['event_description'].str.replace("Description:",'')

# Format political party information
df['political_party'] = df['political_party'].apply(party_formatting)

new_dfrows_list = []

# οι παρακατω λιστες αναφερονται στο πώς μπορεί να ξεκινάει η περιγραφή του event

#παντα με εναν απο τους δυο τροπους ξεκιναει ενας βουλευτης τη θητεια του
#τα παρακατω χρησιμοποιούνται μόνο για εναρξη θητειας
#μπορει να ειναι μονο στο πρωτο γεγονος της κοινοβουλευτικης περιοδου καθε βουλευτη
#οποτε ειναι μονο στη θεση 0 καθε subdf
start_cases = ['aντικατέστησε', #π.χ. aντικατέστησε:δούρουειρήνη(ρένα)αθανασίου(λόγω:παραίτησηςαπότοβουλευτικόαξίωμα)
               'εκλογής',
               ]

# οταν καποιος απεβιωσε ή παραιτήθηκε
end_cases = ['παραίτησηςαπότοβουλευτικόαξίωμα',
             'απεβίωσε'
             ]

# περιπτώσεις που αλλάζει κομμα ενας βουλευτης
change_party_cases = ['προσχώρησης/επανένταξης', #αλλαζει κομμα
                      'ανεξαρτητοποίηση', # εγινε ανεξαρτητοι (εκτος κομματος)
                      'προσχωρησηστηνκ.ο.τησνεασδημοκρατιας',
                      'ετέθηεκτός' # εγινε ανεξαρτητοι (εκτος κομματος)]
                      ]


for name, subdf in df.groupby(['member_name','period_start_date']):
    #define new_subdf?

    rows_num = subdf.shape[0]
    member_name = name_formatting(subdf.iloc[0]['member_name'])
    end_follows = False #refers to change of political party or any of the end cases
    change_follows = False #refers to change of political party

    # one and only either εκλογης or αντικατεστησε
    if rows_num==1:

        if any((subdf.iloc[0]['event_description'].lower()).startswith(s) for s in start_cases):
            member_start_date = subdf.iloc[0]['event_date']
            member_end_date = subdf.iloc[0]['period_end_date']
            political_party = subdf.iloc[0]['political_party']

            new_dfrows_list.append({'member_name': member_name,
                                    'member_start_date': member_start_date,
                                    'member_end_date': member_end_date,
                                    'political_party': political_party,
                                    })
        else:
            print('Probably missing data of case '+name)
            print()

    else:
        # we iterate through rows inversely over time
        for i in range(rows_num-1,-1,-1): # e.g. 4 rows iterates from index 3 to 0

            # απεβιωσε, παραιτηθηκε
            # τοτε προηγειται γεονος απο τα start_cases ή change_party_cases)
            # και απο αυτα θα παρουμε start_date
            if any((subdf.iloc[i]['event_description'].lower()).startswith(e) for e in end_cases):

                member_end_date = subdf.iloc[i]['event_date']
                last_event_date = subdf.iloc[i]['event_date']
                last_political_party = subdf.iloc[i]['political_party']
                end_follows = True
                change_follows = False

            elif any(p in subdf.iloc[i]['event_description'].lower() for p in change_party_cases):

                if end_follows:
                    member_end_date = last_event_date
                elif change_follows:
                    member_end_date = last_event_date - timedelta(days=1)
                else:
                    member_end_date = subdf.iloc[i]['period_end_date']

                member_start_date = subdf.iloc[i]['event_date']
                political_party = subdf.iloc[i]['political_party']

                new_dfrows_list.append({'member_name': member_name,
                                     'member_start_date': member_start_date,
                                     'member_end_date': member_end_date,
                                     'political_party': political_party,
                                      })

                if end_follows:
                    if last_political_party != political_party:
                        print('Error in data of case '+name)

                #update last event
                last_event_date = subdf.iloc[i]['event_date']
                last_political_party = subdf.iloc[i]['political_party']
                end_follows = False
                change_follows = True

            # εκλογης, αντικατεστησε
            elif any((subdf.iloc[i]['event_description'].lower()).startswith(s) for s in start_cases):
                member_start_date = subdf.iloc[i]['event_date']

                if end_follows: #political party and member_end_date have been declared
                    # member_end_date exei dw8ei hdh an akolou8ei telos
                    new_dfrows_list.append({'member_name': member_name,
                                          'member_start_date': member_start_date,
                                          'member_end_date': member_end_date,
                                          'political_party': last_political_party,
                                          })
                elif change_follows:
                    member_end_date = last_event_date - timedelta(days=1)
                    political_party = subdf.iloc[i]['political_party']
                    new_dfrows_list.append({'member_name': member_name,
                                          'member_start_date': member_start_date,
                                          'member_end_date': member_end_date,
                                          'political_party': political_party,
                                          })
                else:
                    print('Error in data of case '+name)

new_df = pd.DataFrame(new_dfrows_list, columns=['member_name', 'member_start_date',
                                                'member_end_date', 'political_party',
                                                ])
# drop activity that ends before 1/1/2014
new_df = new_df[(new_df['member_end_date'].dt.year >= 2014)]

# replace start dates before 2014 with 1/1/2014
new_df['member_start_date'] = np.where(new_df['member_start_date']<'2014-01-01',pd.to_datetime(['2014-01-01']),new_df['member_start_date'])

new_df.to_csv('../out_files/members_data_2014onwards.csv', header=True,index=False, encoding='utf-8')

