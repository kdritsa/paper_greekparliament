# -*- coding: utf-8 -*-
import os 
import csv
import sys
import jellyfish
import re
import math
import argparse
import wordify_number
import pygtrie
from cltk.tokenize.sentence import TokenizeSentence
from cltk.tokenize.word import WordTokenizer
from cltk.corpus.utils.importer import CorpusImporter
from greek_accentuation.syllabify import syllabify

JOINED_SENTENCES_PATTERN = re.compile(r"\.([^\s\.])")

NON_WORDS = {'.', ',', ' '}

def read_6sent_records(filename):
    
    sentiment_records = {}

    with open(filename, encoding='utf-8') as f:
        reader = csv.reader(f)
        next(reader) #skip headers
        for row in reader:
            word = row[1]
            accept = True if row[0] == 'yes' else False
            term = row[2]
            sentiments = row[3:]
            #{'να': {'term': 'να', 'sentiments': [4.33, 4.0, 2.67, 2.0, 1.0, 4.33], 'accept': True}}
            # there is no 'N/A' value in sentiment records                
            sentiment_records[word] = {
                'accept': accept,
                'term': term,
                'sentiments': [ float(s) for s in sentiments ]
            }

    return sentiment_records

def create_sentiments_trie(filename):

    sentiments_trie = pygtrie.CharTrie()

    with open(filename, encoding='utf-8') as f:
        reader = csv.reader(f)
        next(reader) #skip headers
        for row in reader:
            word = row[0]
            sentiments = row[1:]
            sentiments_trie[word] = [ float (s) for s in sentiments ]

    return sentiments_trie

def read_pos_neg_records(filename):
    
    sentiment_records = {}

    with open(filename, encoding='utf-8') as f:
        reader = csv.reader(f)
        next(reader) #skip headers
        for row in reader:

            word = row[1]
            accept = True if row[0] == 'yes' else False
            term = row[2]
            sentiment = row[3]
            #{'διακοπτετε': {'sentiment': '-', 'accept': False, 'term': 'διακοπες'},...}
            sentiment_records[word] = {
                'accept': accept,
                'term': term,
                'sentiment': sentiment
            }

    return sentiment_records

def create_pos_neg_trie(filename):

    pos_neg_trie = pygtrie.CharTrie()

    with open(filename, encoding='utf-8') as f:
        reader = csv.reader(f)
        next(reader) #skip headers
        for row in reader:
            word = row[0]
            sentiment = row[1]
            pos_neg_trie[word] = sentiment

    return pos_neg_trie
 

def smog_index(text):

    text = text.replace('!','.')
    text = re.sub(JOINED_SENTENCES_PATTERN, r". \1", text)    
    tokenizer = TokenizeSentence('greek')
    sentences = tokenizer.tokenize_sentences(text)

    num_sentences = len(sentences)
    if num_sentences < 30:
        return None

    polysyllables = 0
    for sentence in sentences:
        for word in word_tokenizer.tokenize(sentence):
            if word in NON_WORDS:
                continue
            if word.isdigit():
                try:
                    numericals = wordify_number.num_to_text(int(word))
                    for nw in numericals:
                        if len(syllabify(nw)) > 2:
                            polysyllables += 1
                except:
                    # if there is a number over billions, we ignore it
                    continue
            else:
                if len(syllabify(word)) > 2:
                    polysyllables += 1

    smog_grade = 1.0430 * math.sqrt(polysyllables*30/num_sentences) + 3.1291
    smog_grade = int(smog_grade * 100.0) / 100.0 
    return smog_grade

def sent6_vec(text, top400_6pc, trie_6pc):
    
    word_vecs = []

    for text_word in word_tokenizer.tokenize(text):

        # First check in the top 400 most frequent words.
        sentiment_record = top400_6pc.get('text_word', None)

        # If word from text matches exactly with one of the top
        # 400 most frequent words and if we have manually accepted
        # the match.
        if sentiment_record is not None and sentiment_record['accept']:
            word_vecs.append(sentiment_record['sentiments'])
            continue
        
        first_half_word = text_word[0:(math.ceil(len(text_word)/2))]
        max_dist = 0
        best_sentiments = None
#        best_match = None
        
        if not trie_6pc.has_subtrie(first_half_word):
            continue
        
        for term, sentiments in trie_6pc.iteritems(prefix=first_half_word):
            if len(term) > ((2 * len(first_half_word)) + 2):
                continue            
            dist = jellyfish.jaro_winkler(text_word, term)
            if dist > max_dist:
                max_dist = dist
#                best_match = term
                best_sentiments = sentiments #keep sentiments of best match
        
        if max_dist >= 0.97:
#            print('vectors match', text_word, best_match)
            word_vecs.append(best_sentiments)
    
    sums = [ 0 for i in range(6) ]
    nums = [ 0 for i in range(6) ]
    
    # sum of squares for each sentiment of each word
    for word_vec in word_vecs:
        for i, sentiment in enumerate(word_vec):
            sums[i] += word_vec[i]**2.0
            nums[i] += 1    

    # square root of each sum devided by the number of words            
    text_vec = [
        (round(((sums[i]/nums[i])**0.5), 2)) if nums[i] != 0 else 0
        for i in range(6)
    ]

    return text_vec


def pos_neg_counter(text, top400_pos_neg, pos_neg_trie):
    
    word_ratings = []
    
    for text_word in word_tokenizer.tokenize(text):
        
        # First check in the top 400 most frequent words    
        sentiment_record = top400_pos_neg.get('text_word', None)
        
        # if word from text matches exactly with one of the top 400
        # most frequent words and if we have manually accepted the
        # match
        if sentiment_record is not None and sentiment_record['accept']:
            word_ratings.append(sentiment_record['sentiment'])
            continue
        
        first_half_word = text_word[0:(math.ceil(len(text_word)/2))]
        best_sentiment = None
#        best_match = None
        max_dist = 0

        if not pos_neg_trie.has_subtrie(first_half_word):
            continue
        
        for term, sentiment in pos_neg_trie.iteritems(prefix=first_half_word):
            if len(term) > ((2 * len(first_half_word)) + 2):
                continue
            dist = jellyfish.jaro_winkler(text_word, term)
            if dist > max_dist:
                max_dist = dist
                best_sentiment = sentiment # keep sentiment of best match
#                best_match = term
        
        if max_dist >= 0.97:
#            print('pos / neg match', text_word, best_match)
            word_ratings.append(best_sentiment)
    
    pos_sum = 0
    neg_sum = 0
    
    # sum of squares for each sentiment of each word
    for rating in word_ratings:  
        if rating == '+':
            pos_sum += 1
        elif rating == '-':
            neg_sum += 1
            
    return(pos_sum, neg_sum)


parser = argparse.ArgumentParser()
parser.add_argument("csv_input_path")
parser.add_argument("output_folder_path")
args = parser.parse_args()

csv_input_path = args.csv_input_path
new_datapath = args.output_folder_path

word_tokenizer = WordTokenizer('greek')

if not os.path.isdir(new_datapath):
    os.mkdir(new_datapath)
    
filename = os.path.basename(csv_input_path)

corpus_importer = CorpusImporter('greek')
corpus_importer.import_corpus('greek_models_cltk')

new_filename = os.path.splitext(filename)[0]+'_analyzed'+os.path.splitext(filename)[1]
results_file = os.path.join(new_datapath, new_filename)

print('Reading top 400 of 6%...')
top400_6pc = read_6sent_records('top400_6sent_manually_accepted.csv')
print('Done')
print('Reading top 6%...')                                
trie_6pc = create_sentiments_trie('out_lexicon_6sent.csv')
print('Done')
print('Reading top 400 positive / negative...')
top400_pos_neg = read_pos_neg_records('top400_pos_neg_manually_accepted.csv')
print('Done')
print('Reading positive / negative...')
pos_neg_trie = create_pos_neg_trie('out_lexicon_pos_neg.csv')
print('Done')

# csv.field_size_limit(sys.maxsize)

with open(csv_input_path, 'r', encoding = 'utf-8', newline='') as old_csvfile, open(results_file, 'w', encoding = 'utf-8', newline='') as analyzed_csvfile:
    csv_writer = csv.writer(analyzed_csvfile)
    csv_reader = csv.reader(old_csvfile)
    
    for i, row in enumerate(csv_reader):
        if i % 1000 == 0:
            print(i)
        speech = row[4]
        smog = smog_index(speech)
        
        speech = (speech.lower()).translate(str.maketrans('άέόώήίϊΐύϋΰ',
                                                          'αεοωηιιιυυυ'))
        speech = re.sub("[().,;?!:«»“”]",' ', speech)
        speech = re.sub('\s+' , ' ', speech) 
        sent1 = sent6_vec(speech, top400_6pc, trie_6pc)
        sent2 = pos_neg_counter(speech, top400_pos_neg, pos_neg_trie)
        
        new_row = row[0:4]
        new_row.append(smog)
        new_row.append(sent1)
        new_row.append(sent2)

#         If no useful data are provided, do not write row in output file
        if smog==None and sent1==[0, 0, 0, 0, 0, 0] and sent2==(0, 0):
            pass
        else:
            csv_writer.writerow(new_row)
