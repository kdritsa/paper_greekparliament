#!/bin/bash

iterations=2
diter=5
siter=5
vector_size=100
no_rows='all'

PYTHONHASHSEED=0 python compass_stability_1990s_2010s.py --run=0 --iterations=$iterations --diter=$diter --siter=$siter --size=$vector_size --rows=$no_rows
echo 'finished run 0'
PYTHONHASHSEED=0 python compass_stability_1990s_2010s.py --run=1 --iterations=$iterations --diter=$diter --siter=$siter --size=$vector_size --rows=$no_rows
echo 'finished run 1'
python compare_dataframes.py -f1='../out_files/stability_compass_run0_iterations'$iterations'_diter'$diter'_siter'$siter'_size'$vector_size'_rows'$no_rows'.csv' -f2='../out_files/stability_compass_run1_iterations'$iterations'_diter'$diter'_siter'$siter'_size'$vector_size'_rows'$no_rows'.csv'
