# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import operator

fig1 = plt.figure(figsize=(6,5))

##Smog throughout time
#
time = [1991, 1996, 2001, 2006, 2011, 2016]
smog_per_era = [15.57, 15.45, 15.53, 14.8, 14.29, 13.18]
average_smog = [14.76 for i in range(6)]

line1, = plt.plot(time,smog_per_era, '.c-')
line1.set_dashes([2, 2, 10, 2])
plt.scatter(time,smog_per_era, c='c')

#Average
plt.axhline(y=14.77, linestyle='dashdot', color='xkcd:dark grey', alpha=0.5)

plt.xlabel('time', fontsize=12)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.ylabel('smog index', fontsize=12)
plt.title('SMOG index through time', fontsize=15)
plt.show()
fig1.set_size_inches(14, 8)
fig1.savefig('../images/average_smog_through_time.png', dpi=200, bbox_inches='tight')

#************************************************
nd_smog_per_era = [15.55, 15.88, 15.36, 14.94, 14.4, 13.91]
pasok_smog_per_era = [15.45, 15.29, 15.67, 15.09, 15.35, 14.03]
syn_smog_per_era = [16.33, 16.08, None, 17.19, None,None]
independent_smog_per_era = [15.75, None, 16.22, 17.04, 14.53, 10.82]
kke_smog_per_era = [15.69, 15.61, 14.73, 13.38, 12.6, 12.16 ]
ps_smog_per_era = [16.22, 16.23, None, None, None, None]
dikki_smog_per_era = [None, 16.23, 15.33, None, None, None ]
laos_smog_per_era = [None, None, None, 13.16, 13.42, None]
syriza_smog_per_era = [None, None, None, 13.84, 13.63, 13.84]
anel_smog_per_era = [None, None, None, None, 13.84, 13.35]
gd_smog_per_era = [None,None,None,None,12.75,9.69]
dimar_smog_per_era = [None,None,None,None,13.92, 10.27 ]

fig2 = plt.figure(figsize=(9,6))

line1, = plt.plot(time,kke_smog_per_era, '#FF0000', label='ΚΚΕ')
line2, = plt.plot(time,nd_smog_per_era, '.b-', label='ND')
line3, = plt.plot(time,pasok_smog_per_era, '.g-', label='PASOK')
line4, = plt.plot(time,syriza_smog_per_era, '#FFA500', label='SYRIZA')
line5, = plt.plot(time, dimar_smog_per_era,'.m-', label='DIMAR')
line6, = plt.plot(time, anel_smog_per_era, '#20B2AA', label='ANEL - Panos Kammenos')
line7, = plt.plot(time, laos_smog_per_era, '#FFA07A', label='LAOS')
line8 = plt.plot(time, gd_smog_per_era, '.k-', label='Golden Dawn')
line9 = plt.plot(time, dikki_smog_per_era, '#FFFF00', label='DIKKI')
line10 = plt.plot(time, ps_smog_per_era, '#DEB887', label = 'Political Spring')
line11 = plt.plot(time, independent_smog_per_era, '#8B4513', label = 'Independent Members')
line12 = plt.plot(time, syn_smog_per_era, '#00FF00', label = 'SYN')
                  
plt.setp(plt.gca().lines, linewidth=2.5, marker='.')

##Average smog through time
line13 = plt.plot(time, smog_per_era, linestyle='dashed', color='xkcd:dark grey', alpha=0.4, linewidth=3)
#
plt.xlabel('time', fontsize=12)
plt.ylabel('smog index', fontsize=12)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.legend()
plt.title('SMOG index per party through time', fontsize=15)
plt.show()
fig2.set_size_inches(14, 8)
fig2.savefig('../images/smog_per_party_through_time.png', dpi=200, bbox_inches='tight')

#************************************************

fig3 = plt.figure(figsize=(9,6))

average_smog_per_party = {'ND':15.04, 
                          'PASOK':15.38, 
                          'Alternative Ecologists':16.14,
                          'SYN':16.15,
                          'Independent members':13.98,
                          'Political Spring':15.31,
                          'KKE':13.38,
                          'DIKKI':15.75,
                          'LAOS':13.36,
                          'SYRIZA':13.78,
                          'Golden Dawn':10.23,
                          'DIMAR':11.97,
                          'ANEL - Panos Kammenos':13.62,
                          'KKE Interior':15.35,
                          'DIANA':14.61,
                          'Independent Democratic MPs':13.82,
                          'The River':11.98,
                          'DISI':13.26,
                          'Union of Centrists':10.09,
                          'ANEL - NPD Alliance':10.88,
                          'LAE':13.9
                          }
sorted_average = dict(sorted(average_smog_per_party.items(), key=operator.itemgetter(1)))

labels = [key for key,value in sorted_average.items()]

smogs = [value for key,value in sorted_average.items()]

cm = plt.get_cmap('plasma')
#plt.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

index = np.arange(len(labels))
barlist = plt.bar(index, smogs, width=0.7)
plt.xlabel('Political parties', fontsize=12)
plt.ylabel('Average SMOG Index', fontsize=12)
plt.xticks(index, labels, fontsize=12, rotation=80)
plt.yticks(fontsize=12)
plt.title('Average SMOG Index per political party', fontsize=15)

for i in range(len(barlist)):
    barlist[i].set_color(cm(1.0*i/21))

#Average
plt.axhline(y=14.77, linestyle='-', color='xkcd:dark grey', alpha=0.3)
plt.ylim(ymin=9)
#plt.tight_layout()
plt.show()
fig3.set_size_inches(14, 8)
fig3.savefig('../images/average_smog_per_party.png', dpi=200, bbox_inches='tight')