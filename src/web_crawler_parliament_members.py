# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import re
import csv
import datetime

now = datetime.datetime.now()

original_members_data = open('../out_files/original_members_data.csv','w+',
                             encoding='utf-8')
csv_writer = csv.writer(original_members_data, delimiter=',')

_URL = 'http://www.hellenicparliament.gr/Vouleftes/Diatelesantes-Vouleftes-Apo-Ti-Metapolitefsi-Os-Simera/'

response = requests.get(_URL)
html = response.text #.content -> response as bytes, .text-> response as unicode/string
soup = BeautifulSoup(html, "html.parser")

#Get dropdown list
members_dropdown = soup.find("select", id="ctl00_ContentPlaceHolder1_dmps_mpsListId") #.get_text(separator="\n")
members_links={}

#Store all links and members' names in a dictionary
for option in members_dropdown.find_all('option'):
    members_links[option['value']]=option.get_text()

print('Total list length: ', len(members_links))

member_counter=0

for link, member in members_links.items():

    if member!='' and member!=' Επιλέξτε Βουλευτή':

        member_counter+=1
        print(str(member_counter)+' from '+str(len(members_links)))
        print("Name: ",member)
        member_URL = _URL+'?MpId='+link
        print("Processing page ",member_URL,"\n")

        response = requests.get(member_URL)
        html = response.text #.content -> response as bytes, .text-> response as unicode/string
        soup_member = BeautifulSoup(html, "html.parser")

        #if the page has no table
        if not soup_member.find("tbody"):
            original_members_data.write('No:'+str(member_counter)+',Name:'+member+',NO DATA\n')

        else:
            trs = soup_member.find("tbody").find_all("tr", {"class":["odd", "even"]})

            for tr in trs:

                td_columns = [td.getText() for td in tr.find_all("td")] #get text from columns

                period = td_columns[0]
                period = re.sub(r"\s+", "", period) #remove newlines, tabs and whitespaces
                if '-)' in period:
                    # for example Period:ΙΖ΄(20/09/2015-) means it continues up to today
                    # we need the line below for connecting members with their years of parliamentary activity
                    period = re.sub('-\)', '-'+ now.strftime("%d/%m/%Y")+')', period)

                date = td_columns[1]
                date = re.sub(r"\s+", "", date) #remove newlines, tabs and whitespaces

                administrative_region = td_columns[2]
                administrative_region = re.sub(r"\s+", "", administrative_region) #remove newlines, tabs and whitespaces

                parliamentary_party = td_columns[3]
                parliamentary_party = re.sub(r"\s+", "", parliamentary_party) #remove newlines, tabs and whitespaces

                description = td_columns[4]
                description = re.sub(r"\s+", "", description) #remove newlines, tabs and whitespaces

                csv_writer.writerow(['No:'+str(member_counter),
                                     'Name:'+member,
                                     'Period:'+period,
                                     'Date:'+date,
                                     'Administrative-Region:'+administrative_region,
                                     'Parliamentary-Party:'+parliamentary_party,
                                     'Description:'+description])

original_members_data.close()