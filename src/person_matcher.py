import jellyfish
import datetime
import re

starttime = datetime.datetime.now()

match_counter = 0

f2 = open('speakers_data.txt','r+', encoding='utf-8') #encoding for Greek
speakers_data = f2.readlines()

f3 = open('members_data.txt', 'r+', encoding='utf-8') 
members_data = f3.readlines()

# Results of the comparison and best matches
f4 = open('members_speakers_comparison.txt', 'w+', encoding='utf-8')

nickname_in_parenthesis = re.compile(r"(\([α-ω]{2,}\))") #(πανος)

#CLEAN SPEAKERS
for line1 in speakers_data:

    max_dist = 0
    max_pair='null'
    speaker_nickname='null'
    
    speaker_name=line1.split(',')[0]
    speaker_year=line1.split(',')[1]
    
    # Remove 1-2 letter characters
    speaker_name = ' '.join( [word for word in speaker_name.split(' ') if len(word)>2] )

        
    if nickname_in_parenthesis.search(speaker_name):
        speaker_nickname = re.sub('[()]','',(nickname_in_parenthesis.search(speaker_name)).group())
    
    for line2 in members_data:

        member_name_part=line2.split(',')[0]
        
        member_surname = member_name_part.split(' ')[0]
        member_name = member_name_part.split(' ')[2]
        member_year=line2.split(',')[1]
        
        temp_max = 0
        
        if speaker_year==member_year:
        
            #Make comparisons of speaker with members' names and reversed members' names
            dist1 = jellyfish.jaro_winkler(speaker_name, member_name+' '+member_surname)
            dist2 = jellyfish.jaro_winkler(speaker_name, member_surname+' '+member_name)
            
            temp_max = max(temp_max,dist1,dist2)
            
            #We compare speaker with member's nickname and surname
            if nickname_in_parenthesis.search(member_name_part):
                member_nickname = re.sub ('[()]','',(nickname_in_parenthesis.search(member_name_part)).group())
                
                if speaker_nickname=='null':
                    dist3 = jellyfish.jaro_winkler(speaker_name, member_nickname+' '+member_surname)
                    dist4 = jellyfish.jaro_winkler(speaker_name, member_surname+' '+member_nickname)
                else:
                    #afou nickname pantou sto telos
                    dist3 = jellyfish.jaro_winkler(speaker_name, member_name+' '+member_surname+' '+member_nickname)
                    dist4 = jellyfish.jaro_winkler(speaker_name, member_surname+' '+member_name+' '+member_nickname)

                temp_max = max(temp_max, dist3, dist4)                    
                      
            else:           
                if speaker_nickname!='null':
                    # Remove speaker nickname and complete comparison
                    speaker_without_nick = re.sub(r"(\s*\([α-ω]{2,}\)\s*)", '', speaker_name)
                    dist3 = jellyfish.jaro_winkler(speaker_without_nick, member_name+' '+member_surname)
                    dist4 = jellyfish.jaro_winkler(speaker_without_nick, member_surname+' '+member_name)
            
                    temp_max = max(temp_max, dist3, dist4)                    
        
            if temp_max>max_dist:
                max_dist=temp_max
                max_pair =speaker_name+' '+speaker_year+' ----> '+member_name_part+' '+member_year
        
        else:
            #if years don't match, pass this member
            pass;

#    print(max_pair)
#    print(str(max_dist)+'\n')

    if max_dist>0.95:
        match_counter+=1
        print(max_pair)
        print(str(max_dist)+'\n')
        
        f4.write(max_pair+'\n')
        f4.write(str(max_dist)+'\n\n')

f4.close()

print('Matches :'+str(match_counter))

endtime = datetime.datetime.now()
print('Comparison lasted from '+str(starttime)+' until '+str(endtime)+'.')