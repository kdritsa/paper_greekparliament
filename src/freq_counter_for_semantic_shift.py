# -*- coding: utf-8 -*-
from cltk.tokenize.word import WordTokenizer
from collections import defaultdict
import re
from collections import Counter
import operator
import pandas as pd
import time
import nltk

from nltk.tokenize import MWETokenizer
from nltk import word_tokenize

tokenizer = MWETokenizer([
    ('@', 'μερα25'), ('@', 'νδ'), ('@', 'δησυ'), ('@', 'συριζα'),
    ('@', 'ελληνικη_λυση'), ('@', 'πολαν'),
    ('@', 'ανεξαρτητοι_δημοκρατικοι_βουλευτες'),
    ('@', 'ανελ'), ('@', 'δηανα'), ('@', 'κιναλ'), ('@', 'δηκκι'),
    ('@', 'συνασπισμος'), ('@', 'πασοκ'), ('@', 'κκε'),
    ('@', 'λαος'), ('@', 'χα'), ('@', 'οε'), ('@', 'λαε'),
    ('@', 'ποταμι'),('@', 'εκ'), ('@', 'δημαρ'),
    ('@', 'sw')
    ])

word_tokenizer = WordTokenizer('greek')

word_freq = defaultdict(int)

df = pd.read_csv('../out_files/tell_all_cleaned.csv')
df = df[df['speech'].notna()]
df.speech = df.speech.apply(lambda x: x.replace(".", " "))

tell_all_list = df.speech.to_list()

tell_all = ' '.join([l.lower() for l in tell_all_list])

# tell_all = re.sub("\d+", "", tell_all)
tell_all = re.sub("\s\s+" , ' ', tell_all)

# tokenized_tell_all = (word_tokenizer.tokenize(tell_all))
tokenized_tell_all = tokenizer.tokenize(word_tokenize(tell_all))

total_number = len(tokenized_tell_all)
print(total_number)

freqs = Counter(tokenized_tell_all)
print('finished counting')

freqs_df = pd.DataFrame.from_dict(freqs, orient='index', columns = ['frequency'])
freqs_df = freqs_df.reset_index()

freqs_df = freqs_df.rename(columns={'index': 'word'})
mask = (freqs_df['word'].str.len() > 1)
freqs_df = freqs_df.loc[mask]
print('Removed entries with one character.')

freqs_df = freqs_df.sort_values('frequency').reset_index(drop=True)

freqs_df['percentage'] = freqs_df['frequency']/total_number

freqs_df.to_csv('../out_files/freqs_for_semantic_shift_cleaned_data.csv', index=False)