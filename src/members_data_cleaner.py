# -*- coding: utf-8 -*-
import re
from collections import defaultdict

def party_formatting(party):
    if party=='ΑΝΕΞΑΡΤΗΤΟΙΕΛΛΗΝΕΣΕΘΝΙΚΗΠΑΤΡΙΩΤΙΚΗΔΗΜΟΚΡΑΤΙΚΗΣΥΜΜΑΧΙΑ':
        party='ανεξαρτητοι ελληνες εθνικη πατριωτικη δημοκρατικη συμμαχια'
    elif party=='ΑΝΕΞΑΡΤΗΤΟΙΕΛΛΗΝΕΣ-ΠΑΝΟΣΚΑΜΜΕΝΟΣ':
        party='ανεξαρτητοι ελληνες - πανος καμμενος'
    elif party=='ΑΝΕΞΑΡΤΗΤΟΙ':
        party= 'ανεξαρτητοι (εκτος κομματος)'
    elif party=='ΣΥΝΑΣΠΙΣΜΟΣ':
        party='συνασπισμος της αριστερας των κινηματων και της οικολογιας'
    elif party=='ΑΝΕΞΑΡΤΗΤΟΙΔΗΜΟΚΡΑΤΙΚΟΙΒΟΥΛΕΥΤΕΣ':
        party='ανεξαρτητοι δημοκρατικοι βουλευτες'
    elif party=='ΠΟΛ.Α.':
        party='πολιτικη ανοιξη'
    elif party=='ΟΟ.ΕΟ.':
        party='οικολογοι εναλλακτικοι (ομοσπονδια οικολογικων εναλλακτικων οργανωσεων)'
    elif party=='ΔΗ.ΑΝΑ.':
        party= 'δημοκρατικη ανανεωση'
    elif party=='ΔΗ.Κ.ΚΙ.':
        party='δημοκρατικο κοινωνικο κινημα'
    elif party=='ΕΝΩΣΗΚΕΝΤΡΩΩΝ':
        party='ενωση κεντρωων'
    elif party== 'ΝΕΑΔΗΜΟΚΡΑΤΙΑ':
        party='νεα δημοκρατια'
    elif party=='ΛΑ.Ο.Σ.':
        party= 'λαικος ορθοδοξος συναγερμος'
    elif party=='ΛΑΪΚΟΣΣΥΝΔΕΣΜΟΣ-ΧΡΥΣΗΑΥΓΗ':
        party='λαικος συνδεσος - χρυση αυγη'
    elif party=='ΚΟΜΜΟΥΝΙΣΤΙΚΟΚΟΜΜΑΕΛΛΑΔΑΣ':
        party='κομμουνιστικο κομμα ελλαδας'
    elif party=='Κ.Κ.Εσ':
        party='κομμουνιστικο κομμα ελλαδας εσωτερικου'
    elif party=='ΣΥΝΑΣΠΙΣΜΟΣΡΙΖΟΣΠΑΣΤΙΚΗΣΑΡΙΣΤΕΡΑΣ':
        party='συνασπισμος ριζοσπαστικης αριστερας'
    elif party=='ΛΑΪΚΗΕΝΟΤΗΤΑ':
        party='λαικη ενοτητα'
    elif party=='ΠΑ.ΣΟ.Κ.':
        party='πανελληνιο σοσιαλιστικο κινημα'
    elif party== 'ΔΗΜΟΚΡΑΤΙΚΗΑΡΙΣΤΕΡΑ':
        party='δημοκρατικη αριστερα'
    elif party=='ΔΗΜΟΚΡΑΤΙΚΗΣΥΜΠΑΡΑΤΑΞΗ(ΠΑΝΕΛΛΗΝΙΟΣΟΣΙΑΛΙΣΤΙΚΟΚΙΝΗΜΑ-ΔΗΜΟΚΡΑΤΙΚΗΑΡΙΣΤΕΡΑ)':
        party='δημοκρατικη συμπαραταξη (πανελληνιο σοσιαλιστικο κινημα - δημοκρατικη αριστερα)'
    elif party=='ΤΟΠΟΤΑΜΙ':
        party='το ποταμι'
    elif party=='ΕΝΩΣΗΚΕΝΤΡΟΥ-ΝΕΕΣΔΥΝΑΜΕΙΣΕΚ/ΝΔ':
        party='ενωση κεντρου - νεες δυναμεις (ε.κ. - ν.δ.)'
    elif party=='ΕΔΗΚ':
        party='ενωση δημοκρατικου κεντρου (εδηκ)'
    elif party=='ΕΘΝΙΚΗΠΑΡΑΤΑΞΙΣ':
        party='εθνική παράταξη'
    elif party=='ΕΘΝΙΚΗΠΑΡΑΤΑΞΙΣ':
        party='εθνικη παραταξη'
    elif party=='ΝΕΟΦΙΛΕΛΕΥΘΕΡΩΝ':
        party='κομμα νεοφιλελευθερων'
    elif party=='ΕΝΙΑΙΑΔΗΜΟΚΡΑΤΙΚΗΑΡΙΣΤΕΡΑ-Ε.Δ.Α.':
        party='ενιαια δημοκρατικη αριστερα (ε.δ.α.)'
    elif party=='ΣΥΜ/ΧΙΑΠΡ':
        party='συμμαχια προοδευτικων και αριστερων δυναμεων'
    else:
        pass;
    return party

def name_formatting(name):

    name = re.sub(r"(\s*-\s*)|(\sή\s)",'-', name) #when people have two surnames or two names, we glue them together with '-' in the middle
    name = name.translate(str.maketrans('άέόώήίϊΐύϋΰ','αεοωηιιιυυυ')) #remove accents
    name = re.sub(r"\t+" , " ", name) #replace tabs with space
    name = re.sub(r"\s\s+" , " ", name) #replace more than one spaces with one space
    name = re.sub(r"(συζ.\s)",'συζ.', name) #remove space between συζ. and the name of the husband
    name = re.sub("μαρια γλυκερια",'μαρια-γλυκερια', name)
    name = re.sub("χατζη χαβουζ γκαληπ",'χατζη-χαβουζ-γκαληπ', name)
    name = re.sub("μακρη θεοδωρου",'μακρη-θεοδωρου', name)
    name = re.sub("καρα γιουσουφ",'καρα-γιουσουφ', name)
    name = re.sub('χατζη οσμαν','χατζη-οσμαν', name)
    name = re.sub("σαδικ αμετ αμετ σαδηκ",'σαδικ αμετ αμετ', name)
    name = re.sub('ακριτα χα λουκη συλβα-καιτη','ακριτα συζ.λουκη συλβα-καιτη', name)
    name = name.rstrip() #remove trailing space from string
    return name

f = open('../out_files/original_members_data.txt', 'r+', encoding='utf-8') #encoding for Greek
original_members_data = f.read()

#Update members_data file, keep names, years of activity and political parties
f2 = open('../out_files/members_data.txt','w+', encoding='utf-8')

#We create a dictionary of unique names as keys and years of parliamentary activity as values
unique_names_and_years = defaultdict(list)

for line in original_members_data.splitlines():

    if ('NO DATA') not in line:

        # We format data for better comparison
        # and we make sure all names consists of 3 space-separated parts
        segments = line.split(',')

        name = segments[1][5:].lower() #make all lower for best comparison
        name = name_formatting(name)

        member_activity_years = segments[2].split('(')[1]
        date_segments = re.sub("[-()/]" , " ", str(member_activity_years)).split(' ')
        years_list = list(range(int(date_segments[2]),int(date_segments[5])+1))

        party = segments[5][20:]
        party = party_formatting(party)

        for y in years_list:
            if y>1988:
                f2.write(name+','+str(y)+','+party+',\n')
            else:
                pass; #don't save members active before 1989, according to our files

f2.seek(0)
unique_lines = set(f2.readlines())
f2.seek(0)
f2.writelines(unique_lines)
f2.truncate()
f2.close()
f.close()