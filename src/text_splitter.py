# -*- coding: utf-8 -*-

import csv
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("csv_input_path")
parser.add_argument("output_folder_path")
parser.add_argument("splits_no", type=int)
args = parser.parse_args()

n = args.splits_no
csv_input_path = args.csv_input_path
new_datapath = args.output_folder_path

if not os.path.isdir(new_datapath):
    os.mkdir(new_datapath)
    
filename = os.path.basename(csv_input_path)

with open(csv_input_path, 'r', encoding = 'utf-8', newline ='\n') as csvfile:
    csv_reader = csv.reader(csvfile)
    output_files = [ open(os.path.join(new_datapath,
                                       'file_' + str(i) + '.csv', ), 'w', encoding = 'utf-8')
                     for i in range(n) ]
    csv_writers = [ csv.writer(f) for f in output_files ]
    for i, record in enumerate(csv_reader):
        csv_writers[i % n].writerow(record)

for output_file in output_files:
    output_file.close()
