##Speech quality and sentiment analysis on the Hellenic Parliament proceedings

Have you ever wished that you could take an objective and comprehensive look
into what is said and how it is said in politics? Within
this project, we examine the records of the Hellenic Parliament sittings
from 1989 up to 2019 in order to evaluate the speech quality and examine
the palette of sentiments that characterize the communication among its
members. This work initiated during the course of a Master thesis entitled
"Speech quality and sentiment analysis on the Hellenic Parliament
proceedings" at the Athens University of Economics & Business in 2018. The
thesis pdf with extensive information on the project development can be found at
 the bottom of this web page
http://www.pyxida.aueb.gr/index.php?op=view_object&object_id=6387&lang=en.

The project resulted in the creation of a dataset that includes 1,194,407
speeches of Greek parliament members with a total volume of 2.15 GB, that
where exported from 5,118 parliamentary sitting record files and extend
chronologically from 1989 up to 2019. The dataset and more information about
it are [publicly available on
Zenodo](https://zenodo.org/record/2587904#.XRN-Y3uEYmo).

In this project, the **readability** of the speeches is evaluated with the use
 of the [Simple Measure of Gobbledygook (SMOG)
formula](https://en.wikipedia.org/wiki/SMOG), partially adjusted to the Greek
 language. The **sentiment mining** is achieved with the use of two Greek
sentiment lexicons.

Below are listed all the scripts for each phase of the project,
along with a description of their basic functionality. The project is under
development and many changes have been made since its beginning.


## Data Collection

The collection of data is divided into two parts, the retrieval of the
parliament records and the retrieval of the information about the parliament
members. The collection is implemented by the scripts below, in the following
 order:

**web_crawler.py:** Downloads the [records of the Hellenic Parliament](https://www.hellenicparliament.gr/Praktika/Synedriaseis-Olomeleias),
that are available in pdf, doc, docx, text format and/or in-page HTML format.
Very few records are completely missing.

**web_crawler_parliament_members.py:** Downloads all the [parliament members
information](https://www.hellenicparliament.gr/Vouleftes/Diatelesantes-Vouleftes-Apo-Ti-Metapolitefsi-Os-Simera/)
from the parliament website and writes them in the original_members_data.txt.

## Data Preprocessing

The data preprocessing concerns matters of data cleaning and data extraction
from the information downloaded from the parliament website. This process is
implemented by the scripts below, in the following order:

**greek_numerals_to_numbers.py:** Used by file_converter.py to convert Greek
numerals and archaic letters to numbers. Greek numerals and archaic letters
are used by the parliament to enumerate the periods, sittings and sessions
of the proceedings.
 
**file_converter.py:** Converts all downloaded record files to simple text file
format and translates the file names from Greek to English.

**members_data_cleaner.py:** Takes as input the file “original_members_data
.txt” and gives as output the file “members_data.txt”, with cleaned up names
in a useful format for further use.

**Greek_names_crawler.py:** Crawls a
[website](https://www.onomatologio.gr/Ονόματα) with lists of Greek male and
female names and their alternatives. Creates one file with all the names
(greek_names_alternatives.txt) and one file with only those names that have
alternatives (greek_names_alts_only.txt).

**member_speech_matcher.py:** Detects all the speakers and their speeches
from the record files and matches them with any of the officially reported
parliament members from the file “members_data.txt”, using Jaro-Winkler distance
for string similarity and matching the year of the record with the years of
activity of the official parliament member. Is also utilizes a list of Greek
names and their alternatives for better name matching. It gives as an output
the file tell_all.csv, each line of which consists of the following data
columns: member name, sitting date, parliamentary period, parliamentary
session, parliamentary sitting, political party, speaker info, speech.

**fill_proedr_names.py:** Fills in the missing name of a member when we know
their institutional role for the sitting e.g. "CHAIRMAN: blablabla".

**group_speaker_per_sitting.py:** Groups together the speeches of each
member for each sitting. Each line on the “tell_all_speaker_per_sitting.csv”
file  has the following format: member name, sitting date, parliamentary
sitting, political party, all speeches of this speaker at that date and sitting
concatenated. Gives as output the file tell_all_speaker_per_sitting.csv.

## Analysis Tools

- [Lexicon 1: Greek Sentiment Lexicon with 6 sentiment
scores](https://github.com/MKLab-ITI/greek-sentiment-lexicon)   
    The first sentiment lexicon we used is a Greek sentiment lexicon that
    includes 2,315 Greek terms and provides ratings for the sentiments of anger,
    disgust, fear, happiness, sadness and surprise. Based on this lexicon, we
    created a new one with 2607 Greek terms, populated for the three genders
    when available and with some corrections.
    **lexicon_adjuster_6sent.py:** Writes a new sentiment lexicon, namely
    “out_lexicon_6sent.csv”, based on the Greek Sentiment Lexicon. For each
    sentiment, it computes the average score of the four annotators. This
    file is used by the script the_analyst.py.

- [Lexicon 2: Greek Sentiment Lexicon with Positive & Negative Sentiment
scores](https://sites.google.com/site/datascienceslab/projects/multilingualsentiment)   
    The second sentiment lexicon we used was the one we created by concatenating
    two lists of positive and negative Greek words.
    **lexicon_adjuster_pos_neg.py:** Creates the file “out_lexicon_pos_neg
    .csv”, by combining the two lists of words provided in the link above.
    Each row in the output file has a Greek term and the symbol “+” or
    “-” depending on the file it came from. It includes 2702 Greek terms, of
    which the 1066 are positive terms and the 1635 are negative terms. This
    file is used by the script the_analyst.py.

- **wordify_number.py:** Converts digits to Greek text. Useful for implementing
 the SMOG Index calculator for the Greek language.
 
- **count_words.py:** Counts the total amount of words for each of the
queries/speeches/corpus parts under interest. Gives as output the file
“count_words_results.txt”.

- **freq_counter.py:** Gives as an output the “word_frequencies.csv”, that
includes all the unique words of the parliament corpus and their frequencies,
 sorted with descending order.
 
- **top400words.py:** Reads the top 400 most common words of the
“word_frequencies.csv” file and tries to match them with the lexicon entries of
each lexicon. For words with less than 5 characters, the Jaro-Winkler
distance accept limit is 0.97 while for longer words the limit drops at 0.91
. This script gives two output files, the “top400_bestmatch_6sent.csv” file
corresponding to Lexicon 1 and the “top400_bestmatch_pos_neg.csv” file
corresponding to Lexicon 2. Each row of these files has the following
information: the corpus word under investigation, the lexicon term that best
matched the corpus word, the sentiment scores of the lexicon term and an
acceptance index (a yes/no field to be manually edited). Due to the fact
that many matches were not correct, we manually edit the acceptance field
and reject the cases that matched unsuccessfully with a lexicon term. The
final manually edited files are namely:
    - “top400_6sent_manually_accepted.csv”
    - “top400_pos_neg_manually_accepted.csv”  

    These files are used by the script the_analyst.py.

## Analysis Implementation

**the_analyst.py:** Calculates the speech quality and sentiment of each group
 of  speeches and writes the results in the new file
 “tell_all_speaker_per_sitting_analyzed.csv”, that has the following format
 in each line: speaker full name, sitting date, parliamentary sitting,
 political party, SMOG index of grouped speeches, 6-sentiment vector of grouped
 speeches, number of positive words of grouped speeches, number of negative
 words of grouped speeches.
 
**analyst_final_results.py:** Calculates the quadratic average of SMOG indexes
and sentiment vectors as well as the sums of positive and negative words,
based on the results we are interested in exporting.

##Results
The following scripts generate the graphs that represent
the results we are interested in:

- **smog_result_graphs.py**

- **six_sentiments_result_graphs.py**

- **pos_neg_result_graphs.py**

